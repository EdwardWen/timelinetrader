﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TLCClientApiTrade_CLI;
using TLCClientApiQuote_CLI;
using TLCClientApi_CLI;
using TLCClientApiRiskManage_CLI;
using TLCClientApiAdmin_CLI;
using System.Runtime.InteropServices;

using MaterialSkin;
using MaterialSkin.Controls;
using System.Deployment.Application;

namespace TimelineTrader
{


    public partial class Form1 : MaterialForm
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        delegate void SetTextCallback(string text);
        delegate void SetBoolCallback(Boolean isChecked);
        public DataTable quoteData;
        public DataTable capitalData;
        public DataTable orderData;
        public DataTable fillData;
        public DataTable positionData;
        public DataTable marginData;
        public DataTable marginRateData;
        public DataTable feeData;
        public DataTable userData;
        public DataTable loginData;
        public DataTable accountMapData;
        public DataTable investorData; 
        public BindingSource bs_marginRate = new BindingSource();
        public BindingSource bs_margin = new BindingSource(); 
        public BindingSource bs_feeData = new BindingSource();
        public BindingSource bs_positionData = new BindingSource();
        public BindingSource bs_userData = new BindingSource();
        public BindingSource bs_loginData = new BindingSource();
        public BindingSource bs_investorData = new BindingSource();
        public BindingSource bs_orderData = new BindingSource();
        public BindingSource bs_fillData = new BindingSource();

        MyQuoteAdapter quoteAdapter;
        MyRiskManageAdapter riskManageadapter;
        MyTradeAdapter tradeAdapter;
        MyAdminAdapter adminAdapter;   

        Timer timer = new Timer();
        int accountmap_rowSelected;
        int order_rowSelected; 
        int investor_rowSelected;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue100, TextShade.WHITE);

            materialSkinManager.ROBOTO_MEDIUM_10 = new Font("Microsoft YaHei", 13);
            materialSkinManager.ROBOTO_MEDIUM_11 = new Font("Microsoft YaHei", 12);
            materialSkinManager.ROBOTO_MEDIUM_12 = new Font("Microsoft YaHei", 12);
            materialSkinManager.ROBOTO_REGULAR_11 = new Font("Microsoft YaHei", 14);

// materialTabControl1.Appearance = TabAppearance.FlatButtons;
//            materialTabControl1.ItemSize = new Size(0, 1);
//           materialTabControl1.SizeMode = TabSizeMode.Fixed; 

        }



        private void Form1_Load(object sender, EventArgs e)
        {

            this.Hide();

            materialTabControl1.Appearance = TabAppearance.FlatButtons;
            materialTabControl1.ItemSize = new Size(0, 1);
            materialTabControl1.SizeMode = TabSizeMode.Fixed;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;


            investorData = new DataTable();
            investorData.Columns.Add("Account", typeof(string));
            investorData.Columns.Add("Exchange", typeof(ExchangeType));
            investorData.Columns.Add("Speculation", typeof(long));
            investorData.Columns.Add("Arbitrage", typeof(long));
            investorData.Columns.Add("Hedge", typeof(long));
            investorData.Columns.Add("MarketMaker", typeof(long));
            investorData.PrimaryKey = new DataColumn[] { investorData.Columns["Account"] , investorData.Columns["Exchange"] };

            bs_investorData.DataSource = investorData;
            dg_InvestorDetails.DataSource = bs_investorData;

            dg_InvestorDetails.Columns["Account"].HeaderText = "客户代码";
            dg_InvestorDetails.Columns["Account"].ReadOnly = true;
            dg_InvestorDetails.Columns["Exchange"].HeaderText = "交易所";
            dg_InvestorDetails.Columns["Exchange"].ReadOnly = true;
            dg_InvestorDetails.Columns["Speculation"].HeaderText = "投机代码";
            dg_InvestorDetails.Columns["Arbitrage"].HeaderText = "套利代码";
            dg_InvestorDetails.Columns["Hedge"].HeaderText = "套保代码";
            dg_InvestorDetails.Columns["MarketMaker"].HeaderText = "做市商代码";

            quoteData = new DataTable();
            quoteData.Columns.Add("Symbol", typeof(string));
            quoteData.Columns.Add("BidQty", typeof(int));
            quoteData.Columns.Add("BidPrice", typeof(double));
            quoteData.Columns.Add("AskQty", typeof(int));
            quoteData.Columns.Add("AskPrice", typeof(double));
            quoteData.Columns.Add("Position", typeof(int));
            quoteData.Columns.Add("Cancels", typeof(int));

            quoteData.PrimaryKey = new DataColumn[] { quoteData.Columns["Symbol"] };

            marginData = new DataTable();
            marginData.Columns.Add("Time", typeof(string));
            marginData.Columns.Add("Account", typeof(string));
            marginData.Columns.Add("Capital", typeof(double));
            marginData.Columns.Add("Available Margin", typeof(double));
            marginData.Columns.Add("Margin Used", typeof(double));
            marginData.Columns.Add("Open PnL", typeof(double));
            marginData.Columns.Add("Closed PnL", typeof(double));
            marginData.PrimaryKey = new DataColumn[] { marginData.Columns["Account"] };

            bs_margin.DataSource = marginData;
            dg_margin.DataSource = bs_margin;

            dg_margin.Columns["Time"].HeaderText = "时间";
            dg_margin.Columns["Account"].HeaderText = "客户代码";
            dg_margin.Columns["Capital"].HeaderText = "资金";
            dg_margin.Columns["Available Margin"].HeaderText = "可用资金";
            dg_margin.Columns["Margin Used"].HeaderText = "用保证金";
            dg_margin.Columns["Open PnL"].HeaderText = "持仓盈利";
            dg_margin.Columns["Closed PnL"].HeaderText = "盈利";


            marginRateData = new DataTable();
            marginRateData.Columns.Add("Account", typeof(string));
            marginRateData.Columns.Add("Symbol", typeof(string));
            marginRateData.Columns.Add("Open", typeof(double));
            marginRateData.Columns.Add("Close", typeof(double));
            marginRateData.Columns.Add("Long", typeof(double));
            marginRateData.Columns.Add("Short", typeof(double));

            bs_marginRate.DataSource = marginRateData;
            dg_marginrate.DataSource = bs_marginRate;

            dg_marginrate.Columns["Account"].HeaderText = "客户代码";
            dg_marginrate.Columns["Symbol"].HeaderText = "合约代码";
            dg_marginrate.Columns["Open"].HeaderText = "开仓";
            dg_marginrate.Columns["Close"].HeaderText = "平今";
            dg_marginrate.Columns["Long"].HeaderText = "开头";
            dg_marginrate.Columns["Short"].HeaderText = "空头";


            //dg_marginrate.DataSource = marginRateData;

            feeData = new DataTable();
            feeData.Columns.Add("Account", typeof(string));
            feeData.Columns.Add("Symbol", typeof(string));
            feeData.Columns.Add("Open", typeof(double));
            feeData.Columns.Add("Close", typeof(double));
            feeData.Columns.Add("CloseYd", typeof(double));

            bs_feeData.DataSource = feeData; 
            dg_fee.DataSource = bs_feeData;

            dg_fee.Columns["Symbol"].HeaderText = "合约代码";
            dg_fee.Columns["Account"].HeaderText = "客户代码";
            dg_fee.Columns["Open"].HeaderText = "开仓";
            dg_fee.Columns["Close"].HeaderText = "平今";
            dg_fee.Columns["CloseYd"].HeaderText = "平昨";

            fillData = new DataTable();
            fillData.Columns.Add("Time", typeof(string));
            fillData.Columns.Add("Account", typeof(string));
            fillData.Columns.Add("Symbol", typeof(string));
            fillData.Columns.Add("TsOrdId", typeof(long));
            fillData.Columns.Add("FillId", typeof(long));
            fillData.Columns.Add("Side", typeof(string));
            fillData.Columns.Add("Price", typeof(double));
            fillData.Columns.Add("Qty", typeof(int));
            fillData.Columns.Add("PosOffset", typeof(string));
            fillData.Columns.Add("Fees", typeof(double));
            fillData.PrimaryKey = new DataColumn[] { fillData.Columns["TsOrdId"] };

            orderData = new DataTable();
            orderData.Columns.Add("Time", typeof(string));
            orderData.Columns.Add("Account", typeof(string));
            orderData.Columns.Add("Symbol", typeof(string));
            orderData.Columns.Add("TsOrdId", typeof(long));
            orderData.Columns.Add("Side", typeof(string));
            orderData.Columns.Add("Price", typeof(double));
            orderData.Columns.Add("Qty", typeof(int));
            orderData.Columns.Add("CumQty", typeof(int));
            orderData.Columns.Add("TIF", typeof(string));
            orderData.Columns.Add("PosOffset", typeof(string));
            orderData.Columns.Add("Status", typeof(string));

            orderData.PrimaryKey = new DataColumn[] { orderData.Columns["TsOrdId"] };

            bs_orderData.DataSource = orderData;
            dg_orders.DataSource = bs_orderData;

            dg_orders.Columns["Time"].HeaderText = "报单时间";
            dg_orders.Columns["Account"].HeaderText = "客户代码";
            dg_orders.Columns["Symbol"].HeaderText = "合约代码";
            dg_orders.Columns["TsOrdId"].HeaderText = "报单编号";
            dg_orders.Columns["Side"].HeaderText = "买卖";
            dg_orders.Columns["Price"].HeaderText = "报单价格";
            dg_orders.Columns["Qty"].HeaderText = "报单手数";
            dg_orders.Columns["CumQty"].HeaderText = "已成交手数";
            dg_orders.Columns["TIF"].HeaderText = "报单类型";
            dg_orders.Columns["PosOffset"].HeaderText = "开平";
            dg_orders.Columns["Status"].HeaderText = "挂单状态";

            bs_fillData.DataSource = fillData;
            dg_fills.DataSource = bs_fillData;

            dg_fills.Columns["Time"].HeaderText = "成交时间";
            dg_fills.Columns["Account"].HeaderText = "客户代码";
            dg_fills.Columns["Symbol"].HeaderText = "合约代码";
            dg_fills.Columns["TsOrdId"].HeaderText = "报单编号";
            dg_fills.Columns["FillId"].HeaderText = "成交编号";
            dg_fills.Columns["Side"].HeaderText = "买卖";
            dg_fills.Columns["Price"].HeaderText = "成交价格";
            dg_fills.Columns["Qty"].HeaderText = "成交手数";
            dg_fills.Columns["PosOffset"].HeaderText = "开平";
            dg_fills.Columns["Fees"].HeaderText = "手续费";

            positionData = new DataTable();
            positionData.Columns.Add("Time", typeof(string));
            positionData.Columns.Add("Account", typeof(string));
            positionData.Columns.Add("Symbol", typeof(string));
            positionData.Columns.Add("Direction", typeof(PositionDirection));
            positionData.Columns.Add("Position", typeof(int));
            positionData.Columns.Add("PositionCost", typeof(double));
            positionData.Columns.Add("ydPosition", typeof(int));
            positionData.Columns.Add("ydPositionCost", typeof(double));
            //positionData.PrimaryKey = new DataColumn[] { quoteData.Columns["Account"] };

            bs_positionData.DataSource = positionData;
            dg_positions.DataSource = bs_positionData;

            dg_positions.Columns["Time"].HeaderText = "时间";
            dg_positions.Columns["Account"].HeaderText = "客户代码";
            dg_positions.Columns["Symbol"].HeaderText = "合约代码";
            dg_positions.Columns["Direction"].HeaderText = "方向";
            dg_positions.Columns["Position"].HeaderText = "今仓";
            dg_positions.Columns["PositionCost"].HeaderText = "今仓价格";
            dg_positions.Columns["ydPosition"].HeaderText = "昨仓";
            dg_positions.Columns["ydPositionCost"].HeaderText = "昨仓价格";

            userData = new DataTable();
            userData.Columns.Add("SystemId", typeof(string));
            userData.Columns.Add("BrokerId", typeof(string));
            userData.Columns.Add("TraderId", typeof(string));
            userData.Columns.Add("TraderName", typeof(string));
            userData.Columns.Add("TraderDept", typeof(string));
            userData.Columns.Add("TraderPassword", typeof(string));
            userData.Columns.Add("CanQuote", typeof(bool));
            userData.Columns.Add("CanTrade", typeof(bool));
            userData.Columns.Add("CanRisk", typeof(bool));
            userData.Columns.Add("CanAdmin", typeof(bool));
            userData.PrimaryKey = new DataColumn[] { userData.Columns["TraderId"] };

            bs_userData.DataSource = userData;
            dg_users.DataSource = bs_userData;

            dg_users.Columns["SystemId"].HeaderText = "系统编码";
            dg_users.Columns["BrokerId"].HeaderText = "经纪公司编码";
            dg_users.Columns["TraderId"].HeaderText = "用户代码";
            dg_users.Columns["TraderId"].ReadOnly = true;
            dg_users.Columns["TraderName"].HeaderText = "用户名称";
            dg_users.Columns["TraderDept"].HeaderText = "营业部";
            dg_users.Columns["TraderPassword"].HeaderText = "密码";
            dg_users.Columns["CanQuote"].HeaderText = "可收行情";
            dg_users.Columns["CanTrade"].HeaderText = "可交易";
            dg_users.Columns["CanRisk"].HeaderText = "风控";
            dg_users.Columns["CanAdmin"].HeaderText = "管理权限";

            loginData = new DataTable();
            loginData.Columns.Add("time", typeof(string));
            loginData.Columns.Add("Username", typeof(string));
            loginData.Columns.Add("ClientType", typeof(string));
            loginData.Columns.Add("Ip", typeof(string));
            loginData.Columns.Add("Mac", typeof(string));
            loginData.Columns.Add("action", typeof(string));

            bs_loginData.DataSource = loginData;
            dg_loginUsers.DataSource = bs_loginData;

            dg_loginUsers.Columns["time"].HeaderText = "时间";
            dg_loginUsers.Columns["Username"].HeaderText = "用户代码";
            dg_loginUsers.Columns["ClientType"].HeaderText = "用户类型";
            dg_loginUsers.Columns["Ip"].HeaderText = "IP";
            dg_loginUsers.Columns["Mac"].HeaderText = "MAC";
            dg_loginUsers.Columns["action"].HeaderText = "事件";


            accountMapData = new DataTable();
            accountMapData.Columns.Add("Username", typeof(string));
            accountMapData.Columns.Add("Account", typeof(string));
            accountMapData.PrimaryKey = new DataColumn[] { accountMapData.Columns["Username"], accountMapData.Columns["Account"] };
            dg_AccountMap.DataSource = accountMapData;

            dg_AccountMap.Columns["Username"].HeaderText = "用户代码";
            dg_AccountMap.Columns["Account"].HeaderText = "资金账户";


            quoteAdapter = new MyQuoteAdapter(this);
            riskManageadapter = new MyRiskManageAdapter(this);
            tradeAdapter = new MyTradeAdapter(this);
            adminAdapter = new MyAdminAdapter(this);

            string server = string.Concat("tcp://" , Properties.Settings.Default.connectionStr);

            //192.168.1.11:57000Properties.Settings.Default.connectionStr = txt_serverip.Text;

            //string server = "tcp://47.100.125.163:56789";
            quoteAdapter.api.start(server);
            riskManageadapter.api.start(server);
            tradeAdapter.api.start(server);
            adminAdapter.api.start(server);

            timer.Interval = (1 * 1000); // 10 secs
            timer.Start();
            btn_header_accounts.Textcolor = System.Drawing.Color.White;


            DialogResult result;
            using (var loginForm = new LoginForm())
                result = loginForm.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                Application.Exit();
            }


            cmb_username.DisplayMember = "TraderId";
            cmb_username.DataSource = userData;
            
            cmb_account.DisplayMember = "Account";
            cmb_account.DataSource = investorData.DefaultView.ToTable(true, "Account");
            //cmb_account.DataSource = accountMapData.DefaultView.ToTable(true, "Account");

            if (this.Account != null)
            {
                tabControl1.TabPages.Remove(this.Account);
            }
            if (this.TransferMoney != null)
            {
                tabControl1.TabPages.Remove(this.TransferMoney);
            }

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                materialLabel1.Text = string.Format("时间线交易管理端 - v{0}", ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4));
            }
        }

        public void refreshOrders()
        {
            bs_orderData.DataSource = orderData;
            //dg_orders.DataSource = bs_orderData;
            dg_orders.DataSource = orderData;

        }


        public void refreshInvestorDg()
        {
            // bs_investorData.DataSource = investorData;
            // dg_InvestorDetails.DataSource = bs_investorData;
            // dg_InvestorDetails.Refresh();
            timer.Interval = (1 * 1000); // 10 secs
            timer.Start();


        }

        private void dg_AccountMap_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            accountmap_rowSelected = e.RowIndex;
            
        }

        private void dg_Orders_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            order_rowSelected = e.RowIndex;

        }

        private void dg_InvestorDetails_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            investor_rowSelected = e.RowIndex;

        }
        
        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            createNewAccount p = new createNewAccount();
            p.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dg_users_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //accountmap_rowSelected = e.RowIndex;
            
            if (e.RowIndex != -1) {
                if (e.ColumnIndex == dg_users.Rows[e.RowIndex].Cells["CanQuote"].ColumnIndex ||
                    e.ColumnIndex == dg_users.Rows[e.RowIndex].Cells["CanTrade"].ColumnIndex ||
                    e.ColumnIndex == dg_users.Rows[e.RowIndex].Cells["CanRisk"].ColumnIndex ||
                    e.ColumnIndex == dg_users.Rows[e.RowIndex].Cells["CanAdmin"].ColumnIndex)
                {
                    DataGridViewCheckBoxCell chk_quote_cell = (DataGridViewCheckBoxCell)dg_users.Rows[e.RowIndex].Cells["CanQuote"]; //  dg_users.CurrentCell;
                    bool canQuote = (bool)chk_quote_cell.EditedFormattedValue;
                    DataGridViewCheckBoxCell checkbox2 = (DataGridViewCheckBoxCell)dg_users.Rows[e.RowIndex].Cells["CanTrade"]; //  dg_users.CurrentCell;
                    bool canTrade = (bool)checkbox2.EditedFormattedValue;
                    DataGridViewCheckBoxCell checkbox3 = (DataGridViewCheckBoxCell)dg_users.Rows[e.RowIndex].Cells["CanRisk"]; //  dg_users.CurrentCell;
                    bool canRisk = (bool)checkbox3.EditedFormattedValue;
                    DataGridViewCheckBoxCell checkbox4 = (DataGridViewCheckBoxCell)dg_users.Rows[e.RowIndex].Cells["CanAdmin"]; //  dg_users.CurrentCell;
                    bool canAdmin = (bool)checkbox4.EditedFormattedValue;
                    
                    User u = new User { username = dg_users.Rows[e.RowIndex].Cells["TraderId"].Value.ToString(), password = "", permissionQuote = canQuote, permissionTrade = canTrade, permissionRiskManage = canRisk, permissionAdmin = canAdmin };
                    adminAdapter.api.setUser(u);
                }
            }
        }

        private void materialTabSelector1_Click(object sender, EventArgs e)
        {

        }

        
        private void txtFilterCapitalAccount_TextChanged(object sender, EventArgs e)
        {
            bs_margin.Filter = string.Format("Account LIKE '%{0}%'", txtFilterCapitalAccount.Text);

        }

        private void txtFilterMarginSymbol_TextChanged(object sender, EventArgs e)
        {
            bs_marginRate.Filter = string.Format("Symbol LIKE '%{0}%'", txtFilterMarginSymbol.Text);
        }

        private void txt_filterSymPos_TextChanged(object sender, EventArgs e)
        {
            bs_positionData.Filter = string.Format("Symbol LIKE '%{0}%'", txt_filterSymPos.Text);
        }
        
        private void txt_filter_investor_account_TextChanged(object sender, EventArgs e)
        {
            bs_investorData.Filter = string.Format("Account LIKE '%{0}%'", txt_investor_account.Text);
        }

        private void txt_filter_investor_exch_TextChanged(object sender, EventArgs e)
        {
            ExchangeType exchcode = (ExchangeType) Enum.Parse(typeof(ExchangeType), txt_investor_exch.Text);

            bs_investorData.Filter = string.Format("Exchange LIKE '%{0}%'", exchcode);
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btn_openAddUsrPanel_Click(object sender, EventArgs e)
        {
            pnl_addUser.Visible = true;
            //System.Windows.Media.Effects.BlurEffect myBlur = new System.Windows.Media.Effects.BlurEffect();
            //myBlur.Radius = 5;
            //pnl_addUser.eff .Effect = myBlur; 
        }

        private void btn_panelAddUserCancel_Click(object sender, EventArgs e)
        {
            pnl_addUser.Visible = false; 
        }

        private void bunifuFlatButton15_Click(object sender, EventArgs e)
        {
            slide_panel.Show(pnl_addUser);
// pnl_addUser.Visible = true;

        }

        private void btn_panelAddUserCancel_Click_1(object sender, EventArgs e)
        {
            slide_panel.Hide(pnl_addUser);
            //pnl_addUser.Visible = false;

        }

        private void btn_panelAddUserSave_Click(object sender, EventArgs e)
        {
            if (txt_newTraderPass.Text == txt_newTraderPass2.Text)
            {
                User u = new User { username = txt_newTraderId.Text, password = txt_newTraderPass.Text, permissionQuote = chk_quote.Checked, permissionTrade = chk_trade.Checked, permissionRiskManage = chk_riskman.Checked, permissionAdmin = chk_admin.Checked };
                adminAdapter.api.addUser(u);
                lbl_passwordmatch.Visible = false;

            }
            else
            {
                lbl_passwordmatch.Visible = true; 
            }
        }

        private void pnl_addUser_MouseMove(object sender, MouseEventArgs e)
        {
        if (e.Button == MouseButtons.Left)
        {
            ReleaseCapture();
            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

    }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_filter_sysNum_TextChanged(object sender, EventArgs e)
        {
            bs_userData.Filter = string.Format("SystemId LIKE '%{0}%' AND BrokerId LIKE '%{1}%' AND TraderId LIKE '%{2}%' ", txt_filter_sysNum.Text, txt_filter_brokerNum.Text, txt_filter_traderId.Text);
        }

        private void txt_filter_brokerNum_TextChanged(object sender, EventArgs e)
        {
            bs_userData.Filter = string.Format("SystemId LIKE '%{0}%' AND BrokerId LIKE '%{1}%' AND TraderId LIKE '%{2}%' ", txt_filter_sysNum.Text, txt_filter_brokerNum.Text, txt_filter_traderId.Text);

        }

        private void txt_filter_traderId_TextChanged(object sender, EventArgs e)
        {
            bs_userData.Filter = string.Format("SystemId LIKE '%{0}%' AND BrokerId LIKE '%{1}%' AND TraderId LIKE '%{2}%' ", txt_filter_sysNum.Text, txt_filter_brokerNum.Text, txt_filter_traderId.Text);

        }

        private void txt_filter_traderType_TextChanged(object sender, EventArgs e)
        {
            bs_userData.Filter = string.Format("SystemId LIKE '%{0}%' AND BrokerId LIKE '%{1}%' AND TraderId LIKE '%{2}%' ", txt_filter_sysNum.Text, txt_filter_brokerNum.Text, txt_filter_traderId.Text);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnl_addUser.Visible = false;

        }

        private void bunifuFlatButton16_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedIndex = 0;
            btn_header_accounts.selected = true;
            btn_header_trading.selected = false;
            btn_header_fees.selected = false;
            btn_header_capital.selected = false;

            btn_header_accounts.Textcolor = System.Drawing.Color.White;
            btn_header_trading.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_fees.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_capital.Textcolor = System.Drawing.SystemColors.ControlDark;

        }

        private void btn_header_trading_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedIndex = 1;
            btn_header_accounts.selected = false;
            btn_header_trading.selected = true;
            btn_header_fees.selected = false;
            btn_header_capital.selected = false;

            btn_header_accounts.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_trading.Textcolor = System.Drawing.Color.White;
            btn_header_fees.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_capital.Textcolor = System.Drawing.SystemColors.ControlDark;

        }

        private void btn_header_fees_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedIndex = 2;
            btn_header_accounts.selected = false;
            btn_header_trading.selected = false;
            btn_header_fees.selected = true;
            btn_header_capital.selected = false;

            btn_header_accounts.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_trading.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_fees.Textcolor = System.Drawing.Color.White;
            btn_header_capital.Textcolor = System.Drawing.SystemColors.ControlDark;

        }

        private void btn_header_capital_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedIndex = 3;
            btn_header_accounts.selected = false;
            btn_header_trading.selected = false;
            btn_header_fees.selected = false;
            btn_header_capital.selected = true;

            btn_header_accounts.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_trading.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_fees.Textcolor = System.Drawing.SystemColors.ControlDark;
            btn_header_capital.Textcolor = System.Drawing.Color.White;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!pnl_user_Search2.Visible)
            {
                pnl_user.Width = pnl_user_Search2.Width + 4;
                pnl_user_Search2.Visible = true;
                btn_user_collapse.Text = "<<";
                bunifuTransition1.ShowSync(pnl_user);
                bunifuTransition1.ShowSync(pnl_user_Search2);
                //panel14.Width -= pnl_user_Search2.Width; 
            }
            else
            {
                pnl_user.Width = btn_user_collapse.Width +4 ;
                pnl_user_Search2.Visible = false;
                btn_user_collapse.Text = ">>";
                bunifuTransition1.ShowSync(pnl_user);
                //panel14.Width += pnl_user_Search2.Width;

            }
        }

        private void bunifuFlatButton15_Click_1(object sender, EventArgs e)
        {
            slide_panel.Show(pnl_addUser);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            slide_panel.Hide(pnl_addUser);

        }

        private void txt_filterAccountPos_TextChanged(object sender, EventArgs e)
        {
            bs_positionData.Filter = string.Format("Account LIKE '%{0}%'", txt_filterAccountPos.Text);

        }

        private void txtFilterFeeAccount_TextChanged(object sender, EventArgs e)
        {
            bs_feeData.Filter = string.Format("Account LIKE '%{0}%'", txtFilterFeeAccount.Text);

        }

        private void txtFilterFeeSymbol_TextChanged(object sender, EventArgs e)
        {
            bs_feeData.Filter = string.Format("Symbol LIKE '%{0}%'", txtFilterFeeSymbol.Text);

        }

        private void txtFilterOrderAccount_TextChanged(object sender, EventArgs e)
        {
            bs_orderData.Filter = string.Format("Account LIKE '%{0}%'", txtFilterOrderAccount.Text);
        }
        
        private void txtFilterOrderSym_TextChanged(object sender, EventArgs e)
        {
            bs_orderData.Filter = string.Format("Symbol LIKE '%{0}%'", txtFilterOrderSym.Text);
        }

        private void txtFilterFillAccount_TextChanged(object sender, EventArgs e)
        {
            bs_fillData.Filter = string.Format("Account LIKE '%{0}%'", txtFilterFillAccount.Text);
        }

        private void txtFilterFillSym_TextChanged(object sender, EventArgs e)
        {
            bs_fillData.Filter = string.Format("Symbol LIKE '%{0}%'", txtFilterFillSym.Text);
        }


        private void txtFilterUserLogin_TextChanged(object sender, EventArgs e)
        {
            bs_loginData.Filter = string.Format("Username LIKE '%{0}%'", txt_filter_loginUser.Text);
        }

        private void btn_addAccountMap_Click(object sender, EventArgs e)
        {
            adminAdapter.api.addAccountToUser(cmb_account.Text, cmb_username.Text); 
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {
            Console.Write("here");
            String temp_userName = dg_AccountMap.Rows[accountmap_rowSelected].Cells[0].Value.ToString();
            String temp_account = dg_AccountMap.Rows[accountmap_rowSelected].Cells[1].Value.ToString();
            adminAdapter.api.removeAccountFromUser(temp_account, temp_userName);
            /*if (!dg_AccountMap.Rows[this.rowIndex].IsNewRow)
            {
                this.dataGridView1.Rows.RemoveAt(this.rowIndex);
            }*/

        }

        private void orderCancel_Click(object sender, EventArgs e)
        {
            Console.Write("here");
            long orderid = Convert.ToInt64( dg_orders.Rows[order_rowSelected].Cells[3].Value.ToString()) ;
            String account = dg_orders.Rows[order_rowSelected].Cells[1].Value.ToString();
            OrderCancelRequest cancelledorder = new OrderCancelRequest();
            cancelledorder.account = account;
            cancelledorder.id = new OrderId { tsOrdId = orderid } ; 
            tradeAdapter.api.cancelOrder(cancelledorder);

        }

        private void investorDelete_context_Click(object sender, EventArgs e)
        {
            String account = dg_InvestorDetails.Rows[investor_rowSelected].Cells[0].Value.ToString();
            ExchangeType exchcode = (ExchangeType)Enum.Parse(typeof(ExchangeType), dg_InvestorDetails.Rows[investor_rowSelected].Cells[1].Value.ToString());
            /*
            adminAdapter.api.d (new Investor { account = txt_investor_account.Text, exchange = exchcode, tradingCode = tc });

            Console.Write("here");
            long orderid = Convert.ToInt64(dg_orders.Rows[order_rowSelected].Cells[4].Value.ToString());
            String account = dg_orders.Rows[order_rowSelected].Cells[1].Value.ToString();
            OrderCancelRequest cancelledorder = new OrderCancelRequest();
            cancelledorder.account = account;
            cancelledorder.id = new OrderId { tsOrdId = orderid };
            tradeAdapter.api.cancelOrder(cancelledorder);
            */
        }


        private void btnExpInvestor_Click(object sender, EventArgs e)
        {
            if (!pnl_investor_add.Visible)
            {
                pnl_investor.Width = pnl_investor_add.Width + 4;
                pnl_investor_add.Visible = true;
                btnExpInvestor.Text = "<<";
                //bunifuTransition1.ShowSync(pnl_user);
                // bunifuTransition1.ShowSyn(pnl_user_Search2);
            }
            else
            {
                pnl_investor.Width = btnExpInvestor.Width + 4;
                pnl_investor_add.Visible = false;
                btnExpInvestor.Text = ">>";
                //                bunifuTransition1.ShowSync(pnl_user);
            }

        }

        private void btn_investor_add_Click(object sender, EventArgs e)
        {
            pnl_add_investor.Visible = true;
            lbl_save_investor_error.Visible = false;

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExchangeType exchcode = (ExchangeType)Enum.Parse(typeof(ExchangeType), txt_investor_exch.Text);
            bs_investorData.Filter = string.Format("convert(Exchange, 'System.String') Like '%{0}%' ", (int)exchcode);

        }

        private void btn_investor_add_Click_1(object sender, EventArgs e)
        {
            try
            {
                TradingCode tc = new TradingCode();

                if (txt_investor_arbitrage_add.Text != "")
                    tc.arbitrage = Convert.ToInt64(txt_investor_arbitrage_add.Text);
                if (txt_investor_hedge_add.Text != "")
                    tc.hedge = Convert.ToInt64(txt_investor_hedge_add.Text);
                if (txt_investor_mm_add.Text != "")
                    tc.marketMaker = Convert.ToInt64(txt_investor_mm_add.Text);
                if (txt_investor_speculate_add.Text != "")
                    tc.speculation = Convert.ToInt64(txt_investor_speculate_add.Text);
                ExchangeType exchcode = (ExchangeType)Enum.Parse(typeof(ExchangeType), cmb_investor_exch_add.Text);
                adminAdapter.api.addInvestor(new Investor { account = txt_investor_account_add.Text, exchange = exchcode, tradingCode = tc });
                lbl_save_investor_error.Visible = false;

            }
            catch (Exception ex)

            {
                lbl_save_investor_error.Visible = true;
                lbl_save_investor_error.Text = "代码必须是数字"; 
            }

        }

        private void btn_close_addInvestor_Click(object sender, EventArgs e)
        {
            pnl_add_investor.Visible = false; 
        }

        private void dg_InvestorDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dg_InvestorDetails.Rows[e.RowIndex];

            Investor investor = new TLCClientApi_CLI.Investor
            {
                account = row.Cells["Account"].Value.ToString(),
                exchange = (ExchangeType)Enum.Parse(typeof(ExchangeType), row.Cells["Exchange"].Value.ToString()),
                tradingCode = new TradingCode {
                    speculation = Int64.Parse(row.Cells["Speculation"].Value.ToString()),
                    arbitrage = Int64.Parse(row.Cells["Arbitrage"].Value.ToString()),
                    hedge = Int64.Parse(row.Cells["Hedge"].Value.ToString()),
                    marketMaker = Int64.Parse(row.Cells["MarketMaker"].Value.ToString()),
                }
            };
            adminAdapter.api.setInvestor(investor);
        }

        private void pnl_user_Paint(object sender, PaintEventArgs e)
        {

        }
    }

    class MyQuoteAdapter : QuoteSpiWrapper
    {
        public QuoteApiWrapper api;
        Form1 mainWindow;

        public MyQuoteAdapter(Form1 main)
        {
            api = new QuoteApiWrapper(this);
            mainWindow = main;

        }
        public override void onConnected()
        {
            Console.WriteLine("Connected quote");
            LoginRequest r = new LoginRequest
            {
                username = "jchen",
                password = "abc123"
            };

            api.login(r);
        }

        public override void onDisconnected()
        {
            Console.WriteLine("onDisconnected");
        }

        public override void onMarketBook(MarketBook marketBook)
        {
            try
            {
                Console.WriteLine("onMarketBook symbol={0} bidDepth={1} askDepth={2} bid={3}@{4} ask={5}@{6}", marketBook.symbol, marketBook.bidDepth, marketBook.askDepth, marketBook.bids[0].qty, marketBook.bids[0].price, marketBook.asks[0].qty, marketBook.asks[0].price);
                //mainWindow.SetAccountPassword(marketBook.symbol);
                //mainWindow.SetLabel2(marketBook.bids[0].price.ToString());

                // TODO: should run this on UI thread
                DataRow foundRow = mainWindow.quoteData.Rows.Find(marketBook.symbol);

                if (foundRow == null)
                {

                    mainWindow.quoteData.Rows.Add(marketBook.symbol, marketBook.bids[0].qty, marketBook.bids[0].price, marketBook.asks[0].qty, marketBook.asks[0].price, 0, 0);

                }
                else
                {
                    foundRow["BidQty"] = marketBook.bids[0].qty;
                    foundRow["BidPrice"] = marketBook.bids[0].price; ;
                    foundRow["AskQty"] = marketBook.asks[0].qty;
                    foundRow["AskPrice"] = marketBook.asks[0].price;
                    //mainWindow.quoteData.AcceptChanges();

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Press <ENTER> to exit.");
            }
        }

        public override void onRspLogin(LoginReply reply)
        {
            //Console.WriteLine("onRspLogin ret={0} user={1} account={2}", reply.ret, reply.username, reply.account);
            //api.subscribe("T1806", QuoteType.MarketBook);
            //api.subscribe("m1809", QuoteType.Trade);
            //api.subscribe("TF1806", QuoteType.MarketBook);
//            api.subscribe("a1809", QuoteType.Trade);
        }

        public override void onRspLogout(LogoutReply reply)
        {
            Console.WriteLine("onRspLogout ret={0}", reply.ret);
        }

        public override void onRspSubscribe(SubscribeReply reply)
        {
            Console.WriteLine("onRspSubscribe ret={0} symbol={1} quoteType={2}", reply.ret, reply.symbol, reply.quoteType);
        }

        public override void onTrade(Trade trade)
        {
            Console.WriteLine("onTrade symbol={0} {1}@{2} volume={3} openInterest={4}", trade.symbol, trade.tick.qty, trade.tick.price, trade.volume, trade.openInterest);

        }


    }

    class MyRiskManageAdapter : RiskManageSpiWrapper
    {
        public RiskManageApiWrapper api;
        Form1 mainWindow;

        public MyRiskManageAdapter(Form1 main)
        {
            api = new RiskManageApiWrapper(this);
            mainWindow = main;

        }


        public override void onConnected()
        {
            Console.WriteLine("Connected risk");
            mainWindow.dg_margin.Invoke((Action)delegate ()
            {
                mainWindow.dg_margin.ClearSelection();
                mainWindow.marginData.Clear();
                mainWindow.dg_marginrate.ClearSelection();
                mainWindow.marginRateData.Clear();
                mainWindow.dg_fee.ClearSelection();
                mainWindow.feeData.Clear();

                LoginRequest r = new LoginRequest
                {
                    username = "jchen",
                    password = "abc123"
                };


                api.login(r);
            });
            
        }

        public override void onDisconnected()
        {
            Console.WriteLine("onDisconnected");
        }

        public override void onRspLogin(LoginReply reply)
        {
            Console.WriteLine("onRspLogin ret={0} user={1}", reply.ret, reply.username);
            api.qryUserAccountMapping();
        }

        public override void onRspLogout(LogoutReply reply)
        {
            Console.WriteLine("onRspLogout ret={0}", reply.ret);
        }

        public override void onRspQryUserAccountMapping(string user, string account, bool isLast)
        {
            if (isLast) return;
            Console.WriteLine("onRspQryUserAccountMapping user={0}, account={1}, isLast={2}", user, account, isLast);
            api.qryCapitalInformation(new AccountRequest { account = account });
            api.qryFlowInformation(new AccountRequest { account = account });
            api.qryRiskManageParameter(new AccountRequest { account = account });
            api.setRiskManageParameter(new RiskManageParameter { account = account, paramName = "maxSymbolCancelCount", paramValue = "380" });
            api.qryMarginRate(new AccountRequest { account = account });
            //api.setMarginRate(new MarginRate { account = account, product = "cu", symbol = "cu1812", openMargin = 0.005, closeMargin = 0.005, longMargin = 0.005, shortMargin = 0.005 });
            api.qryCommissionRate(new AccountRequest { account = account });
            //api.setCommissionRate(new CommissionRate { account = account, product = "cu", symbol = "cu1812", commissionMode = CommissionMode.ByVolume, openCommission = 10, closeYdCommission = 10, closeCommission = 20 });
        }

        public override void onRspQryCapitalInformation(CapitalInformation capital, bool isLast)
        {
            if (!isLast)
            {
                Console.WriteLine("onRspQryCapitalInformation capital=[{0}, {1}, {2}, {3} ,{4}, {5}], isLast={6}", capital.account, capital.capital, capital.margin, capital.frozenMargin, capital.openPnL, capital.closedPnL, isLast);

                mainWindow.dg_margin.Invoke((Action)delegate ()
                {
                    DataRow foundRow = mainWindow.marginData.Rows.Find(capital.account);
                    String timeStamp = DateTime.Now.ToString("hh:mm:ss");
                    if (foundRow == null)
                    {
                        mainWindow.marginData.Rows.Add(timeStamp, capital.account, capital.capital, capital.margin, capital.frozenMargin, capital.openPnL, capital.closedPnL);
                    }
                    else
                    {
                        foundRow["Time"] = timeStamp;
                        foundRow["Capital"] = capital.capital;
                        foundRow["Available Margin"] = capital.margin;
                        foundRow["Margin Used"] = capital.frozenMargin;
                        foundRow["Open PnL"] = capital.openPnL;
                        foundRow["Closed PnL"] = capital.closedPnL;

                    }

                    mainWindow.marginData.AcceptChanges();
                });

                //DataRow foundRow = mainWindow.orderData.Rows.Find(fill.id.clOrdId);

                //if (fill.qty == (int)foundRow["Qty"])
            }
        }

        public override void onRspQryFlowInformation(FlowInformation reply, bool isLast)
        {
            if (isLast) return;
            Console.WriteLine("onRspQryFlowInformation reply=[{0}, {1}, {2}, {3}], isLast={4}", reply.account, reply.symbol, reply.orderCount, reply.cancelCount, isLast);
        }

        public override void onRspQryMarginRate(MarginRate rate, bool isLast)
        {
            if (!isLast)
            {
                //Console.WriteLine("onRspQryMarginRate rate=[{0}, {1}, {2}, {3}, {4}, {5}], isLast={6}", rate.account, rate.symbol, rate.openMargin, rate.closeMargin, rate.longMargin, rate.shortMargin, isLast);
                mainWindow.dg_marginrate.Invoke((Action)delegate ()
                {
                    mainWindow.marginRateData.Rows.Add(rate.account, rate.symbol, rate.openMargin, rate.closeMargin, rate.longMargin, rate.shortMargin);
                });
            }

        }

        public override void onRspSetMarginRate(ReturnType result, MarginRate rate)
        {
            Console.WriteLine("onRspSetMarginRate result ={0}, rate=[{1}, {2}, {3}, {4}, {5}, {6}]", result, rate.account, rate.symbol, rate.openMargin, rate.closeMargin, rate.longMargin, rate.shortMargin);
        }

        public override void onRspQryCommissionRate(CommissionRate rate, bool isLast)
        {
            if (isLast) return;
            //Console.WriteLine("onRspQryCommissionRate rate=[{0}, {1}, {2}, {3}, {4}], isLast={5}", rate.account, rate.symbol, rate.openCommission, rate.closeYdCommission, rate.closeCommission, isLast);
            
            mainWindow.dg_fee.Invoke((Action)delegate ()
            {
                mainWindow.feeData.Rows.Add(rate.account, rate.symbol, rate.openCommission, rate.closeYdCommission, rate.closeCommission);
            });

        }

        public override void onRspSetCommissionRate(ReturnType result, CommissionRate rate)
        {
            Console.WriteLine("onRspSetCommissionRate result ={0}, rate=[{1}, {2}, {3}, {4}, {5}]", result, rate.account, rate.symbol, rate.openCommission, rate.closeYdCommission, rate.closeCommission);
        }

        public override void onRspQryRiskManageParameter(RiskManageParameter parameter, bool isLast)
        {
            if (isLast) return;
            Console.WriteLine("onRspQryRiskManageParameter reply=[{0}, {1}, {2}], isLast={3}", parameter.account, parameter.paramName, parameter.paramValue, isLast);
        }

        public override void onRspSetRiskManageParameter(ReturnType result, RiskManageParameter parameter)
        {
            Console.WriteLine("onRspSetRiskManageParameter result={0}, parameter=[{1}, {2}, {3}]", result, parameter.account, parameter.paramName, parameter.paramValue);
        }


    };

    class MyTradeAdapter : TradeSpiWrapper
    {
        Form1 mainWindow;
        public TradeApiWrapper api;
        public MyTradeAdapter(Form1 main)
        {
            api = new TradeApiWrapper(this);
            mainWindow = main;

        }

        public override void onConnected()
        {
            Console.WriteLine("Connected trade");
            mainWindow.dg_margin.Invoke((Action)delegate ()
            {
                mainWindow.dg_orders.ClearSelection();
                mainWindow.orderData.Clear();
                mainWindow.dg_fills.ClearSelection();
                mainWindow.fillData.Clear();
                mainWindow.dg_positions.ClearSelection();
                mainWindow.positionData.Clear();

                LoginRequest r = new LoginRequest
                {
                    username = "jchen",
                    password = "abc123"
                };

                api.login(r);
            });
            
        }

        public override void onDisconnected()
        {
            Console.WriteLine("onDisconnected");
        }

        public override void onRspLogin(LoginReply reply)
        {
            Console.WriteLine("onRspLogin ret={0} user={1}", reply.ret, reply.username);
            //OrderRequest orderRequest = new OrderRequest
            //{
            //    clOrdId = 5566,
            //    account = "12345",
            //    symbol = "ru1809",
            //    timeInForce = TimeInForce.IOC,
            //    side = Side.Bid,
            //    qty = 1,
            //    price = 10000,
            //    positionOffset = PositionOffset.CloseLast
            //};

            //api.newOrder(orderRequest);
            api.qryUserAccountMapping();
        }

        public override void onRspLogout(LogoutReply reply)
        {
            Console.WriteLine("onRspLogout ret={0}", reply.ret);
        }

        public override void onErrorRtnCancelOrder(Reject reply)
        {
            Console.WriteLine("onErrorRtnCancelOrder clOrdId={0} tsOrdId={1}", reply.id.clOrdId, reply.id.tsOrdId);
        }

        public override void onRspCancelOrder(Reply reply)
        {
            Console.WriteLine("onRspCancelOrder clOrdId={0} tsOrdId={1}", reply.id.clOrdId, reply.id.tsOrdId);
        }

        public override void onRspQryUserAccountMapping(string user, string account, bool isLast)
        {
            if (isLast) return;
            Console.Write("onRspQryUserAccountMapping user={0}, account={1}, isLast={2}", user, account, isLast);
            AccountRequest a = new AccountRequest { account = account }; 
            api.qryOrder(a);
            api.qryPosition(a);
            api.qryFill(a); 
            

        }

        public override void onRspNewOrder(Reply reply)
        {
            Console.WriteLine("onRspNewOrder clOrdId={0} tsOrdId={1}", reply.id.clOrdId, reply.id.tsOrdId);
        }

        public override void onRspQryFill(Fill fill, bool isLast)
        {
            if (isLast) return;

            mainWindow.dg_fills.Invoke((Action)delegate ()
            {
                DataRow foundRow2 = mainWindow.fillData.Rows.Find(fill.id.tsOrdId);
                if (foundRow2 != null) return;

                Console.WriteLine("onRspQryFill clOrdId={0} tsOrdId={1} isLast={2}", fill.id.clOrdId, fill.id.tsOrdId, isLast);


                DataRow foundRow = mainWindow.orderData.Rows.Find(fill.id.tsOrdId);

                if (foundRow != null)
                {
                    mainWindow.fillData.Rows.Add(fill.fillTime, foundRow["Account"], foundRow["Symbol"], fill.id.tsOrdId, fill.fillId, foundRow["side"], fill.price, fill.qty, foundRow["PosOffset"], fill.commission);
                }
                else
                {
                    mainWindow.fillData.Rows.Add(fill.fillTime, fill.account, "", fill.id.tsOrdId, fill.fillId, 0, fill.price, fill.qty, "平", fill.commission);
                }
            });
        }

        public override void onRspQryOrder(Order order, bool isLast)
        {

            if (!isLast) {
                Console.WriteLine("onRspQryOrder clOrdId={0} tsOrdId={1} isLast={2}", order.reply.id.clOrdId, order.reply.id.tsOrdId, isLast);

                mainWindow.dg_orders.Invoke((Action)delegate ()
                {
                    DataRow foundRow = mainWindow.orderData.Rows.Find(order.reply.id.tsOrdId);
                    if (foundRow == null)
                    {
                        foundRow = mainWindow.orderData.Rows.Add(order.sendTime, order.reply.account, order.symbol, order.reply.id.tsOrdId, order.side, order.price, order.qty, order.cumulativeQty, order.timeInForce, order.positionOffset, order.status);
                    }
                    else
                    {
                        foundRow.BeginEdit();
                        foundRow["Status"] = order.status;
                        foundRow["CumQty"] = order.cumulativeQty;
                        foundRow.EndEdit();
                    }
                });
            }
        }

        public override void onErrorRtnNewOrder(OrderReject reply)
        {
            Console.WriteLine("onErrorRtnNewOrder clOrdId={0} tsOrdId={1} reason={2}", reply.reject.id.clOrdId, reply.reject.id.tsOrdId, reply.reject.reason);
            mainWindow.dg_orders.Invoke((Action)(() => mainWindow.orderData.Rows.Add(reply.sendTime, reply.reject.account, reply.symbol, reply.reject.id.tsOrdId, reply.side, reply.price, reply.qty, 0, reply.timeInForce, reply.positionOffset, reply.status)));

            //           int a = 1;
        }

        public override void onRtnNewOrder(Order reply)
        {
            Console.WriteLine("onRspNewOrder clOrdId={0} tsOrdId={1}", reply.reply.id.clOrdId, reply.reply.id.tsOrdId);
            mainWindow.dg_orders.Invoke((Action)(() => mainWindow.orderData.Rows.Add(reply.sendTime, reply.reply.account, reply.symbol, reply.reply.id.tsOrdId, reply.side, reply.price, reply.qty, 0, reply.timeInForce, reply.positionOffset, reply.status)));

//            DataRow row = mainWindow.orderData.Rows.Add(reply.sendTime, reply.reply.account, reply.symbol, reply.reply.id.tsOrdId, reply.side, reply.price, reply.qty, 0, reply.timeInForce, reply.positionOffset, reply.status);
//            mainWindow.dg_orders.Invoke((Action)(() => mainWindow.refreshOrders()));
        }

        public override void onRtnCancelOrder(Reply reply)
        {
            Console.WriteLine("onRtnCancelOrder clOrdId={0} tsOrdId={1}", reply.id.clOrdId, reply.id.tsOrdId);
            mainWindow.dg_orders.Invoke((Action)delegate ()
            {
                DataRow foundRow = mainWindow.orderData.Rows.Find(reply.id.tsOrdId);

                if (foundRow != null)
                {
                    foundRow["Status"] = OrderStatus.Cancelled;
                    mainWindow.orderData.AcceptChanges();
                }
            });
        }

        public override void onRtnOrderFill(Order order, Fill fill)
        {
            mainWindow.dg_orders.Invoke((Action)delegate ()
            {
                DataRow foundRow = mainWindow.orderData.Rows.Find(fill.id.tsOrdId);
                foundRow["Status"] = order.status;
                foundRow["CumQty"] = order.cumulativeQty;
                mainWindow.orderData.AcceptChanges();

                mainWindow.dg_fills.Invoke((Action)(() => mainWindow.fillData.Rows.Add(fill.fillTime, fill.account, order.symbol, fill.id.tsOrdId, fill.fillId, foundRow["side"], fill.price, fill.qty, foundRow["PosOffset"], fill.commission)));
            });
        }

        public override void onRspQryPosition (  Position pos, bool isLast)
        {
            if (isLast) return;
            mainWindow.dg_positions.Invoke((Action)(() => mainWindow.positionData.Rows.Add(DateTime.Now.ToString("hh:mm:ss"), pos.account, pos.symbol, pos.direction, pos.position, pos.positionCost, pos.ydPosition, pos.ydPositionCost)));
        }
    }

    class MyAdminAdapter : AdminSpiWrapper
    {
        public AdminApiWrapper api;
        Form1 mainWindow;
        public MyAdminAdapter(Form1 main)
        {
            api = new AdminApiWrapper(this);
            mainWindow = main;
        }

        public override void onConnected()
        {
            mainWindow.dg_users.Invoke((Action)delegate()
            {
                mainWindow.dg_users.ClearSelection();
                mainWindow.userData.Clear();
                mainWindow.dg_AccountMap.ClearSelection();
                mainWindow.accountMapData.Clear();
                mainWindow.dg_InvestorDetails.ClearSelection();
                mainWindow.investorData.Clear();
                mainWindow.dg_loginUsers.ClearSelection();
                mainWindow.loginData.Clear();

                LoginRequest r = new LoginRequest
                {
                    username = "jchen",
                    password = "abc123"
                };


                api.login(r);
            });

            Console.WriteLine("Connected admin");
            
        }

        public override void onDisconnected()
        {
            Console.WriteLine("onDisconnected");
        }

        public override void onRspAddUser(ReturnType result, User user)
        {
            Console.WriteLine("onRspAddUser result={0}, user=[{1}, {2}, {3}, {4}, {5}, {6}]", result, user.username, user.password, user.permissionQuote, user.permissionTrade, user.permissionRiskManage, user.permissionAdmin);
            mainWindow.dg_users.Invoke((Action)delegate ()
            {
                DataRow foundRow = mainWindow.userData.Rows.Find(new object[] { user.username });
                if (foundRow == null)
                {
                    mainWindow.dg_users.Invoke((Action)(() => mainWindow.userData.Rows.Add("1111", "2222", user.username, "hello", "prop", user.password, user.permissionQuote, user.permissionTrade, user.permissionRiskManage, user.permissionAdmin)));
                }
            });
        }

        public override void onRspLogin(LoginReply reply)
        {
            Console.WriteLine("onRspLogin ret={0} user={1}", reply.ret, reply.username);
            api.qryUserAccountMapping("");
            //User u = new User { username = "foo", password = "bar", permissionQuote = true, permissionTrade = true, permissionRiskManage = true, permissionAdmin = true };
            //api.addUser(u);
            api.qryUsers();
            api.qryLoginUsers();
            api.qryUserEvent();
            //u.password = "barbar";
            //api.setUser(u);
            //api.addAccountToUser("999999", "jchen");
            //api.removeAccountFromUser("999999", "jchen");
            api.qryInvestors();
            //Investor investor = new Investor { account = "99999", exchange = ExchangeType.INE };
            //investor.tradingCode = new TradingCode();
            //investor.tradingCode.speculation = 1;
            //investor.tradingCode.arbitrage = 2;
            //investor.tradingCode.hedge = 3;
            //investor.tradingCode.marketMaker = 4;
            //api.addInvestor(investor);
            //investor.tradingCode.marketMaker = 777;
            //api.setInvestor(investor);
        }

        public override void onRspLogout(LogoutReply reply)
        {
            Console.WriteLine("onRspLogout ret={0}", reply.ret);
        }

        public override void onRspQryUserAccountMapping(string user, string account, bool isLast)
        {
            if (isLast) return;

            Console.WriteLine("onRspQryUserAccountMapping user={0} account={1}, isLast={2}", user, account, isLast);
            mainWindow.dg_AccountMap.Invoke((Action)(() => mainWindow.accountMapData.Rows.Add(user, account)));

        }

        public override void onRspQryLoginUsers(LoginUser user, bool isLast)
        {
            if (isLast) return;
            
            Console.WriteLine("onRspQryLoginUsers user=[{0}, {1}, {2}, {3}, {4}] isLast={5}", user.username, user.clientType, user.ip, user.mac, user.loginTime, isLast);
            
            //mainWindow.loginData.Rows.Add(user.username, user.clientType, user.ip, user.mac, user.loginTime);
        }

        public override void onRspQryUserEvent(UserEvent userEvent, bool isLast)
        {
            Console.WriteLine("onRspQryUserEvent user=[{0}, {1}, {2}, {3}, {4}, {5}] isLast={6}", userEvent.username, userEvent.clientType, userEvent.ip, userEvent.mac, userEvent.time, userEvent.action, isLast);
            if (isLast) return;
            mainWindow.dg_loginUsers.Invoke((Action)(() => mainWindow.loginData.Rows.Add(userEvent.time, userEvent.username, userEvent.clientType, userEvent.ip, userEvent.mac, userEvent.action)));
        }

        public override void onRspQryUser(User user, bool isLast)
        {
            if (isLast)
            {
                //mainWindow.cmb_username.DataSource = mainWindow.userData.DefaultView.ToTable(true, "TraderId");
                return;
            }

            {  
                Console.WriteLine("onRspQryUser isLast={0}, user=[{1}, {2}, {3}, {4}, {5}, {6}]", isLast, user.username, user.password, user.permissionQuote, user.permissionTrade, user.permissionRiskManage, user.permissionAdmin);
                mainWindow.dg_users.Invoke((Action)(() => mainWindow.userData.Rows.Add("1111", "2222", user.username, "hello", "prop", user.password, user.permissionQuote, user.permissionTrade, user.permissionRiskManage, user.permissionAdmin)));
            }
        }

        public override void onRspAddAccountToUser(ReturnType result, String user, String account)
        {

            Console.WriteLine("onRspAddAccountToUser result={0}, user={1}", result, user);
            if (result == ReturnType.SUCCESS)
            {
                mainWindow.dg_AccountMap.Invoke((Action)(() => mainWindow.accountMapData.Rows.Add(user, account)));
            }
        }

        public override void onRspRemoveAccountFromUser(ReturnType result, String user, String account)
        {
            Console.WriteLine("onRspRemoveAccountFromUser result={0}, user={1} account={2}", result, user, account);
            DataRow foundRow = mainWindow.accountMapData.Rows.Find(new object[] { user, account });
            if (foundRow != null)
            {
                mainWindow.dg_AccountMap.Invoke((Action)(() => mainWindow.accountMapData.Rows.Remove(foundRow)));
            }
        }

        public override void onRspSetUser(ReturnType result, User user)
        {
            Console.WriteLine("onRspSetUser result={0}, user=[{1}, {2}, {3}, {4}, {5}, {6}]", result, user.username, user.password, user.permissionQuote, user.permissionTrade, user.permissionRiskManage, user.permissionAdmin);
        }

        public override void onRspAddInvestor(ReturnType result, Investor investor)
        {
            Console.WriteLine("onRspAddInvestor result={0}, investor=[{1}, {2}, {3}, {4}, {5}, {6}]", result, investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker);
            if (result != ReturnType.SUCCESS) return;

            mainWindow.dg_InvestorDetails.Invoke((Action)delegate ()
            {
                DataRow foundRow = mainWindow.investorData.Rows.Find(new object[] { investor.account, investor.exchange });
                
                if (foundRow == null)
                {
                    mainWindow.investorData.Rows.Add(investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker);
                }
                else
                {
                    foundRow["Hedge"] = investor.tradingCode.hedge;
                    foundRow["MarketMaker"] = investor.tradingCode.marketMaker;
                    foundRow["Arbitrage"] = investor.tradingCode.arbitrage;
                    foundRow["Speculation"] = investor.tradingCode.speculation;
                }
                mainWindow.refreshInvestorDg();
                mainWindow.cmb_account.Invoke((Action)(() => mainWindow.cmb_account.DataSource = mainWindow.investorData.DefaultView.ToTable(true, "Account")));
            });
        }

        public override void onRspSetInvestor(ReturnType result, Investor investor)
        {
            Console.WriteLine("onRspSetInvestor result={0}, investor=[{1}, {2}, {3}, {4}, {5}, {6}]", result, investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker);
        }

        public override void onRspQryInvestors(Investor investor, bool isLast)
        {
            if (isLast) return;
            Console.WriteLine("onRspQryInvestors investor=[{0}, {1}, {2}, {3}, {4}, {5}] isLast={6}", investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker, isLast);
            mainWindow.dg_InvestorDetails.Invoke((Action)(() => mainWindow.investorData.Rows.Add(investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker)));

            //mainWindow.investorData.Rows.Add(investor.account, investor.exchange, investor.tradingCode.speculation, investor.tradingCode.arbitrage, investor.tradingCode.hedge, investor.tradingCode.marketMaker);
        }
    };


}

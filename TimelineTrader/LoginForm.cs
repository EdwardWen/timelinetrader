﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;


namespace TimelineTrader
{
    public partial class LoginForm : Form
    {
        public string myLoginIp = "";

        public LoginForm()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue100, TextShade.WHITE);

            materialSkinManager.ROBOTO_MEDIUM_10 = new Font("Microsoft YaHei", 13);
            materialSkinManager.ROBOTO_MEDIUM_11 = new Font("Microsoft YaHei", 12);
            materialSkinManager.ROBOTO_MEDIUM_12 = new Font("Microsoft YaHei", 12);
            materialSkinManager.ROBOTO_REGULAR_11 = new Font("Microsoft YaHei", 14);
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
// myLoginIp = "192.168.1.11:57000";
            txt_serverip.Text = Properties.Settings.Default.connectionStr; 

        }


        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
//            DialogResult.ToString(txt_serverip.Text)


        }

        private void btn_close_login_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void loginIp_TextChanged(object sender, EventArgs e)
        {
            myLoginIp = txt_serverip.Text;
            Properties.Settings.Default.connectionStr = txt_serverip.Text;
            Properties.Settings.Default.Save();

        }
    }
}

﻿namespace TimelineTrader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.header = new System.Windows.Forms.Panel();
            this.materialLabel1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelAnimator = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_header_capital = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_header_fees = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_header_trading = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_header_accounts = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.materialTabControl1 = new System.Windows.Forms.TabControl();
            this.account1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.Trader = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.pnl_addUser = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.chk_riskman = new System.Windows.Forms.CheckBox();
            this.chk_quote = new System.Windows.Forms.CheckBox();
            this.chk_trade = new System.Windows.Forms.CheckBox();
            this.chk_admin = new System.Windows.Forms.CheckBox();
            this.lbl_passwordmatch = new System.Windows.Forms.Label();
            this.btn_panelAddUserSave = new Bunifu.Framework.UI.BunifuFlatButton();
            this.txt_newTraderId = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_newTraderType = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_newTraderPass2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txt_newTraderPass = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txt_newTraderBroker = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_newTraderDept = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_newTraderName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_newTraderSystemNum = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.dg_users = new System.Windows.Forms.DataGridView();
            this.pnl_user = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.btn_user_collapse = new System.Windows.Forms.Button();
            this.pnl_user_Search2 = new System.Windows.Forms.Panel();
            this.bunifuFlatButton15 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.txt_filter_traderId = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_filter_brokerNum = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_filter_sysNum = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Investor = new System.Windows.Forms.TabPage();
            this.pnl_dg_investor = new System.Windows.Forms.Panel();
            this.pnl_add_investor = new System.Windows.Forms.Panel();
            this.btn_close_addInvestor = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lbl_save_investor_error = new System.Windows.Forms.Label();
            this.cmb_investor_exch_add = new System.Windows.Forms.ComboBox();
            this.txt_investor_mm_add = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt_investor_hedge_add = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txt_investor_arbitrage_add = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_investor_speculate_add = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.btn_investor_add = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label61 = new System.Windows.Forms.Label();
            this.txt_investor_account_add = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.dg_InvestorDetails = new System.Windows.Forms.DataGridView();
            this.ctx_investor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pnl_investor = new System.Windows.Forms.Panel();
            this.pnl_investor_add = new System.Windows.Forms.Panel();
            this.txt_investor_exch = new System.Windows.Forms.ComboBox();
            this.btn_investor_open_add = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_investor_account = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnExpInvestor = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dg_AccountMap = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeleteRow = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.btn_addAccountMap = new Bunifu.Framework.UI.BunifuFlatButton();
            this.cmb_account = new System.Windows.Forms.ComboBox();
            this.cmb_username = new System.Windows.Forms.ComboBox();
            this.bunifuFlatButton11 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton12 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel20 = new System.Windows.Forms.Panel();
            this.txt_filter_loginUser = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.dg_loginUsers = new System.Windows.Forms.DataGridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.TransferMoney = new System.Windows.Forms.TabPage();
            this.Account = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.bunifuFlatButton4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel6 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel9 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bunifuMetroTextbox2 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuMetroTextbox1 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDropdown2 = new Bunifu.Framework.UI.BunifuDropdown();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDropdown1 = new Bunifu.Framework.UI.BunifuDropdown();
            this.bunifuSwitch1 = new Bunifu.Framework.UI.BunifuSwitch();
            this.panel2 = new System.Windows.Forms.Panel();
            this.materialTabSelector2 = new MaterialSkin.Controls.MaterialTabSelector();
            this.trading = new System.Windows.Forms.TabPage();
            this.materialTabControl2 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_orders = new System.Windows.Forms.TabPage();
            this.dg_orders = new System.Windows.Forms.DataGridView();
            this.orderCancel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtFilterOrderSym = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtFilterOrderAccount = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tab_fills = new System.Windows.Forms.TabPage();
            this.dg_fills = new System.Windows.Forms.DataGridView();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtFilterFillSym = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.txtFilterFillAccount = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.tab_positions = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.dg_positions = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txt_filterSymPos = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_filterAccountPos = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tab_capital = new System.Windows.Forms.TabPage();
            this.dg_margin = new System.Windows.Forms.DataGridView();
            this.panel19 = new System.Windows.Forms.Panel();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtFilterCapitalAccount = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tab_fees = new System.Windows.Forms.TabPage();
            this.dg_fee = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtFilterFeeSymbol = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtFilterFeeAccount = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tab_margin = new System.Windows.Forms.TabPage();
            this.dg_marginrate = new System.Windows.Forms.DataGridView();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtFilterMarginSymbol = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilterMarginAccount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tab_moneytransfer = new System.Windows.Forms.TabPage();
            this.materialTabSelector1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.费管理 = new System.Windows.Forms.TabPage();
            this.bunifuFlatButton17 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.doubleBitmapControl1 = new BunifuAnimatorNS.DoubleBitmapControl();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker4 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker5 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker6 = new System.ComponentModel.BackgroundWorker();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer_Tick = new System.Windows.Forms.Timer(this.components);
            this.slide_panel = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuDragControl2 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuDragControl3 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.header.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel1.SuspendLayout();
            this.materialTabControl1.SuspendLayout();
            this.account1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Trader.SuspendLayout();
            this.panel14.SuspendLayout();
            this.pnl_addUser.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_users)).BeginInit();
            this.pnl_user.SuspendLayout();
            this.pnl_user_Search2.SuspendLayout();
            this.Investor.SuspendLayout();
            this.pnl_dg_investor.SuspendLayout();
            this.pnl_add_investor.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_InvestorDetails)).BeginInit();
            this.ctx_investor.SuspendLayout();
            this.pnl_investor.SuspendLayout();
            this.pnl_investor_add.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_AccountMap)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_loginUsers)).BeginInit();
            this.panel8.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.Account.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.trading.SuspendLayout();
            this.materialTabControl2.SuspendLayout();
            this.tab_orders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_orders)).BeginInit();
            this.orderCancel.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tab_fills.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_fills)).BeginInit();
            this.panel17.SuspendLayout();
            this.tab_positions.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_positions)).BeginInit();
            this.panel9.SuspendLayout();
            this.tab_capital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_margin)).BeginInit();
            this.panel19.SuspendLayout();
            this.tab_fees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_fee)).BeginInit();
            this.panel3.SuspendLayout();
            this.tab_margin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_marginrate)).BeginInit();
            this.panel13.SuspendLayout();
            this.费管理.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.SuspendLayout();
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.SteelBlue;
            this.header.Controls.Add(this.materialLabel1);
            this.header.Controls.Add(this.button1);
            this.panelAnimator.SetDecoration(this.header, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.header, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.header, BunifuAnimatorNS.DecorationType.None);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Font = new System.Drawing.Font("BankGothic Lt BT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(2321, 57);
            this.header.TabIndex = 1;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.materialLabel1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.materialLabel1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.materialLabel1, BunifuAnimatorNS.DecorationType.None);
            this.materialLabel1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialLabel1.ForeColor = System.Drawing.Color.White;
            this.materialLabel1.Location = new System.Drawing.Point(12, 9);
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(206, 31);
            this.materialLabel1.TabIndex = 2;
            this.materialLabel1.Text = "时间线交易管理端";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(2276, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "briefcase.png");
            this.imageList1.Images.SetKeyName(1, "cash.png");
            this.imageList1.Images.SetKeyName(2, "cloud-computing.png");
            this.imageList1.Images.SetKeyName(3, "coins.png");
            this.imageList1.Images.SetKeyName(4, "credit-cards-payment.png");
            this.imageList1.Images.SetKeyName(5, "database.png");
            this.imageList1.Images.SetKeyName(6, "employee.png");
            this.imageList1.Images.SetKeyName(7, "search.png");
            this.imageList1.Images.SetKeyName(8, "settings (1).png");
            this.imageList1.Images.SetKeyName(9, "settings.png");
            this.imageList1.Images.SetKeyName(10, "user (1).png");
            this.imageList1.Images.SetKeyName(11, "user (2).png");
            this.imageList1.Images.SetKeyName(12, "user (3).png");
            this.imageList1.Images.SetKeyName(13, "user.png");
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // panelAnimator
            // 
            this.panelAnimator.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.panelAnimator.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 0F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 0;
            animation3.Padding = new System.Windows.Forms.Padding(0);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.panelAnimator.DefaultAnimation = animation3;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.Control;
            this.panel11.Controls.Add(this.btn_header_capital);
            this.panel11.Controls.Add(this.btn_header_fees);
            this.panel11.Controls.Add(this.btn_header_trading);
            this.panel11.Controls.Add(this.btn_header_accounts);
            this.panelAnimator.SetDecoration(this.panel11, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel11, BunifuAnimatorNS.DecorationType.None);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 57);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(130, 980);
            this.panel11.TabIndex = 15;
            // 
            // btn_header_capital
            // 
            this.btn_header_capital.Activecolor = System.Drawing.Color.SteelBlue;
            this.btn_header_capital.BackColor = System.Drawing.SystemColors.Control;
            this.btn_header_capital.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_header_capital.BorderRadius = 0;
            this.btn_header_capital.ButtonText = "   资金管理";
            this.btn_header_capital.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_header_capital, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_header_capital, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_header_capital, BunifuAnimatorNS.DecorationType.None);
            this.btn_header_capital.DisabledColor = System.Drawing.Color.Gray;
            this.btn_header_capital.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_capital.ForeColor = System.Drawing.Color.SteelBlue;
            this.btn_header_capital.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_header_capital.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_header_capital.Iconimage")));
            this.btn_header_capital.Iconimage_right = null;
            this.btn_header_capital.Iconimage_right_Selected = null;
            this.btn_header_capital.Iconimage_Selected = null;
            this.btn_header_capital.IconMarginLeft = 0;
            this.btn_header_capital.IconMarginRight = 0;
            this.btn_header_capital.IconRightVisible = false;
            this.btn_header_capital.IconRightZoom = 0D;
            this.btn_header_capital.IconVisible = false;
            this.btn_header_capital.IconZoom = 90D;
            this.btn_header_capital.IsTab = true;
            this.btn_header_capital.Location = new System.Drawing.Point(0, 297);
            this.btn_header_capital.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btn_header_capital.Name = "btn_header_capital";
            this.btn_header_capital.Normalcolor = System.Drawing.SystemColors.Control;
            this.btn_header_capital.OnHovercolor = System.Drawing.SystemColors.Control;
            this.btn_header_capital.OnHoverTextColor = System.Drawing.Color.Black;
            this.btn_header_capital.selected = false;
            this.btn_header_capital.Size = new System.Drawing.Size(140, 70);
            this.btn_header_capital.TabIndex = 3;
            this.btn_header_capital.Text = "   资金管理";
            this.btn_header_capital.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_header_capital.Textcolor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_header_capital.TextFont = new System.Drawing.Font("Microsoft YaHei", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_capital.Visible = false;
            this.btn_header_capital.Click += new System.EventHandler(this.btn_header_capital_Click);
            // 
            // btn_header_fees
            // 
            this.btn_header_fees.Activecolor = System.Drawing.Color.SteelBlue;
            this.btn_header_fees.BackColor = System.Drawing.SystemColors.Control;
            this.btn_header_fees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_header_fees.BorderRadius = 0;
            this.btn_header_fees.ButtonText = "   费率管理";
            this.btn_header_fees.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_header_fees, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_header_fees, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_header_fees, BunifuAnimatorNS.DecorationType.None);
            this.btn_header_fees.DisabledColor = System.Drawing.Color.Gray;
            this.btn_header_fees.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_fees.ForeColor = System.Drawing.Color.SteelBlue;
            this.btn_header_fees.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_header_fees.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_header_fees.Iconimage")));
            this.btn_header_fees.Iconimage_right = null;
            this.btn_header_fees.Iconimage_right_Selected = null;
            this.btn_header_fees.Iconimage_Selected = null;
            this.btn_header_fees.IconMarginLeft = 0;
            this.btn_header_fees.IconMarginRight = 0;
            this.btn_header_fees.IconRightVisible = false;
            this.btn_header_fees.IconRightZoom = 0D;
            this.btn_header_fees.IconVisible = false;
            this.btn_header_fees.IconZoom = 90D;
            this.btn_header_fees.IsTab = true;
            this.btn_header_fees.Location = new System.Drawing.Point(0, 228);
            this.btn_header_fees.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btn_header_fees.Name = "btn_header_fees";
            this.btn_header_fees.Normalcolor = System.Drawing.SystemColors.Control;
            this.btn_header_fees.OnHovercolor = System.Drawing.SystemColors.Control;
            this.btn_header_fees.OnHoverTextColor = System.Drawing.Color.Black;
            this.btn_header_fees.selected = false;
            this.btn_header_fees.Size = new System.Drawing.Size(140, 70);
            this.btn_header_fees.TabIndex = 2;
            this.btn_header_fees.Text = "   费率管理";
            this.btn_header_fees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_header_fees.Textcolor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_header_fees.TextFont = new System.Drawing.Font("Microsoft YaHei", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_fees.Visible = false;
            this.btn_header_fees.Click += new System.EventHandler(this.btn_header_fees_Click);
            // 
            // btn_header_trading
            // 
            this.btn_header_trading.Activecolor = System.Drawing.Color.SteelBlue;
            this.btn_header_trading.BackColor = System.Drawing.SystemColors.Control;
            this.btn_header_trading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_header_trading.BorderRadius = 0;
            this.btn_header_trading.ButtonText = "   交易查取";
            this.btn_header_trading.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_header_trading, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_header_trading, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_header_trading, BunifuAnimatorNS.DecorationType.None);
            this.btn_header_trading.DisabledColor = System.Drawing.Color.Gray;
            this.btn_header_trading.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_trading.ForeColor = System.Drawing.Color.SteelBlue;
            this.btn_header_trading.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_header_trading.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_header_trading.Iconimage")));
            this.btn_header_trading.Iconimage_right = null;
            this.btn_header_trading.Iconimage_right_Selected = null;
            this.btn_header_trading.Iconimage_Selected = null;
            this.btn_header_trading.IconMarginLeft = 0;
            this.btn_header_trading.IconMarginRight = 0;
            this.btn_header_trading.IconRightVisible = false;
            this.btn_header_trading.IconRightZoom = 0D;
            this.btn_header_trading.IconVisible = false;
            this.btn_header_trading.IconZoom = 90D;
            this.btn_header_trading.IsTab = true;
            this.btn_header_trading.Location = new System.Drawing.Point(0, 159);
            this.btn_header_trading.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btn_header_trading.Name = "btn_header_trading";
            this.btn_header_trading.Normalcolor = System.Drawing.SystemColors.Control;
            this.btn_header_trading.OnHovercolor = System.Drawing.SystemColors.Control;
            this.btn_header_trading.OnHoverTextColor = System.Drawing.Color.Black;
            this.btn_header_trading.selected = false;
            this.btn_header_trading.Size = new System.Drawing.Size(140, 70);
            this.btn_header_trading.TabIndex = 1;
            this.btn_header_trading.Text = "   交易查取";
            this.btn_header_trading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_header_trading.Textcolor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_header_trading.TextFont = new System.Drawing.Font("Microsoft YaHei", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_trading.Click += new System.EventHandler(this.btn_header_trading_Click);
            // 
            // btn_header_accounts
            // 
            this.btn_header_accounts.Activecolor = System.Drawing.Color.SteelBlue;
            this.btn_header_accounts.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_header_accounts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_header_accounts.BorderRadius = 0;
            this.btn_header_accounts.ButtonText = "   客户管理";
            this.btn_header_accounts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_header_accounts, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_header_accounts, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_header_accounts, BunifuAnimatorNS.DecorationType.None);
            this.btn_header_accounts.DisabledColor = System.Drawing.Color.Gray;
            this.btn_header_accounts.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_accounts.ForeColor = System.Drawing.Color.Transparent;
            this.btn_header_accounts.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_header_accounts.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_header_accounts.Iconimage")));
            this.btn_header_accounts.Iconimage_right = null;
            this.btn_header_accounts.Iconimage_right_Selected = null;
            this.btn_header_accounts.Iconimage_Selected = null;
            this.btn_header_accounts.IconMarginLeft = 0;
            this.btn_header_accounts.IconMarginRight = 0;
            this.btn_header_accounts.IconRightVisible = false;
            this.btn_header_accounts.IconRightZoom = 0D;
            this.btn_header_accounts.IconVisible = false;
            this.btn_header_accounts.IconZoom = 90D;
            this.btn_header_accounts.IsTab = true;
            this.btn_header_accounts.Location = new System.Drawing.Point(0, 90);
            this.btn_header_accounts.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btn_header_accounts.Name = "btn_header_accounts";
            this.btn_header_accounts.Normalcolor = System.Drawing.SystemColors.Control;
            this.btn_header_accounts.OnHovercolor = System.Drawing.SystemColors.Control;
            this.btn_header_accounts.OnHoverTextColor = System.Drawing.Color.Black;
            this.btn_header_accounts.selected = true;
            this.btn_header_accounts.Size = new System.Drawing.Size(140, 70);
            this.btn_header_accounts.TabIndex = 0;
            this.btn_header_accounts.Text = "   客户管理";
            this.btn_header_accounts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_header_accounts.Textcolor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_header_accounts.TextFont = new System.Drawing.Font("Microsoft YaHei", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_header_accounts.Click += new System.EventHandler(this.bunifuFlatButton16_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.materialTabControl1);
            this.panelAnimator.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(130, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2191, 980);
            this.panel1.TabIndex = 16;
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.materialTabControl1.Controls.Add(this.account1);
            this.materialTabControl1.Controls.Add(this.trading);
            this.materialTabControl1.Controls.Add(this.费管理);
            this.materialTabControl1.Controls.Add(this.tabPage11);
            this.panelAnimator.SetDecoration(this.materialTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.materialTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.materialTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.materialTabControl1.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.materialTabControl1.ItemSize = new System.Drawing.Size(50, 50);
            this.materialTabControl1.Location = new System.Drawing.Point(0, 0);
            this.materialTabControl1.Multiline = true;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.Padding = new System.Drawing.Point(3, 6);
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(2191, 980);
            this.materialTabControl1.TabIndex = 14;
            // 
            // account1
            // 
            this.account1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.account1.Controls.Add(this.tabControl1);
            this.account1.Controls.Add(this.panel2);
            this.slide_panel.SetDecoration(this.account1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.account1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.account1, BunifuAnimatorNS.DecorationType.None);
            this.account1.ImageIndex = 12;
            this.account1.Location = new System.Drawing.Point(54, 4);
            this.account1.Name = "account1";
            this.account1.Padding = new System.Windows.Forms.Padding(3);
            this.account1.Size = new System.Drawing.Size(2133, 972);
            this.account1.TabIndex = 0;
            this.account1.Text = "客户管理";
            this.account1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Trader);
            this.tabControl1.Controls.Add(this.Investor);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.TransferMoney);
            this.tabControl1.Controls.Add(this.Account);
            this.panelAnimator.SetDecoration(this.tabControl1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.tabControl1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tabControl1, BunifuAnimatorNS.DecorationType.None);
            this.tabControl1.Depth = 0;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(3, 45);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(2127, 877);
            this.tabControl1.TabIndex = 11;
            // 
            // Trader
            // 
            this.Trader.Controls.Add(this.panel14);
            this.Trader.Controls.Add(this.pnl_user);
            this.slide_panel.SetDecoration(this.Trader, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.Trader, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.Trader, BunifuAnimatorNS.DecorationType.None);
            this.Trader.Location = new System.Drawing.Point(4, 36);
            this.Trader.Margin = new System.Windows.Forms.Padding(4);
            this.Trader.Name = "Trader";
            this.Trader.Padding = new System.Windows.Forms.Padding(4);
            this.Trader.Size = new System.Drawing.Size(2119, 837);
            this.Trader.TabIndex = 1;
            this.Trader.Text = "用户管理";
            this.Trader.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.pnl_addUser);
            this.panel14.Controls.Add(this.dg_users);
            this.panelAnimator.SetDecoration(this.panel14, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel14, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel14, BunifuAnimatorNS.DecorationType.None);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(62, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(2053, 829);
            this.panel14.TabIndex = 133;
            // 
            // pnl_addUser
            // 
            this.pnl_addUser.BackColor = System.Drawing.Color.SteelBlue;
            this.pnl_addUser.Controls.Add(this.button2);
            this.pnl_addUser.Controls.Add(this.panel10);
            this.pnl_addUser.Controls.Add(this.label24);
            this.panelAnimator.SetDecoration(this.pnl_addUser, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_addUser, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_addUser, BunifuAnimatorNS.DecorationType.None);
            this.pnl_addUser.Location = new System.Drawing.Point(144, 160);
            this.pnl_addUser.Name = "pnl_addUser";
            this.pnl_addUser.Size = new System.Drawing.Size(985, 407);
            this.pnl_addUser.TabIndex = 10;
            this.pnl_addUser.Visible = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(940, 7);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(38, 38);
            this.button2.TabIndex = 145;
            this.button2.Text = "X";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.chk_riskman);
            this.panel10.Controls.Add(this.chk_quote);
            this.panel10.Controls.Add(this.chk_trade);
            this.panel10.Controls.Add(this.chk_admin);
            this.panel10.Controls.Add(this.lbl_passwordmatch);
            this.panel10.Controls.Add(this.btn_panelAddUserSave);
            this.panel10.Controls.Add(this.txt_newTraderId);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Controls.Add(this.txt_newTraderType);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.txt_newTraderPass2);
            this.panel10.Controls.Add(this.label19);
            this.panel10.Controls.Add(this.txt_newTraderPass);
            this.panel10.Controls.Add(this.label21);
            this.panel10.Controls.Add(this.txt_newTraderBroker);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.txt_newTraderDept);
            this.panel10.Controls.Add(this.label11);
            this.panel10.Controls.Add(this.txt_newTraderName);
            this.panel10.Controls.Add(this.label12);
            this.panel10.Controls.Add(this.txt_newTraderSystemNum);
            this.panel10.Controls.Add(this.label13);
            this.panelAnimator.SetDecoration(this.panel10, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel10, BunifuAnimatorNS.DecorationType.None);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 55);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(985, 352);
            this.panel10.TabIndex = 144;
            // 
            // chk_riskman
            // 
            this.chk_riskman.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.chk_riskman, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.chk_riskman, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.chk_riskman, BunifuAnimatorNS.DecorationType.None);
            this.chk_riskman.FlatAppearance.BorderSize = 0;
            this.chk_riskman.Location = new System.Drawing.Point(760, 175);
            this.chk_riskman.Name = "chk_riskman";
            this.chk_riskman.Size = new System.Drawing.Size(78, 31);
            this.chk_riskman.TabIndex = 169;
            this.chk_riskman.Text = "风控";
            this.chk_riskman.UseVisualStyleBackColor = true;
            // 
            // chk_quote
            // 
            this.chk_quote.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.chk_quote, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.chk_quote, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.chk_quote, BunifuAnimatorNS.DecorationType.None);
            this.chk_quote.FlatAppearance.BorderSize = 0;
            this.chk_quote.Location = new System.Drawing.Point(631, 175);
            this.chk_quote.Name = "chk_quote";
            this.chk_quote.Size = new System.Drawing.Size(78, 31);
            this.chk_quote.TabIndex = 168;
            this.chk_quote.Text = "行情";
            this.chk_quote.UseVisualStyleBackColor = true;
            // 
            // chk_trade
            // 
            this.chk_trade.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.chk_trade, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.chk_trade, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.chk_trade, BunifuAnimatorNS.DecorationType.None);
            this.chk_trade.FlatAppearance.BorderSize = 0;
            this.chk_trade.Location = new System.Drawing.Point(631, 223);
            this.chk_trade.Name = "chk_trade";
            this.chk_trade.Size = new System.Drawing.Size(78, 31);
            this.chk_trade.TabIndex = 167;
            this.chk_trade.Text = "交易";
            this.chk_trade.UseVisualStyleBackColor = true;
            // 
            // chk_admin
            // 
            this.chk_admin.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.chk_admin, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.chk_admin, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.chk_admin, BunifuAnimatorNS.DecorationType.None);
            this.chk_admin.FlatAppearance.BorderSize = 0;
            this.chk_admin.Location = new System.Drawing.Point(760, 223);
            this.chk_admin.Name = "chk_admin";
            this.chk_admin.Size = new System.Drawing.Size(98, 31);
            this.chk_admin.TabIndex = 166;
            this.chk_admin.Text = "管理员";
            this.chk_admin.UseVisualStyleBackColor = true;
            // 
            // lbl_passwordmatch
            // 
            this.lbl_passwordmatch.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.lbl_passwordmatch, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.lbl_passwordmatch, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.lbl_passwordmatch, BunifuAnimatorNS.DecorationType.None);
            this.lbl_passwordmatch.ForeColor = System.Drawing.Color.Red;
            this.lbl_passwordmatch.Location = new System.Drawing.Point(275, 311);
            this.lbl_passwordmatch.Name = "lbl_passwordmatch";
            this.lbl_passwordmatch.Size = new System.Drawing.Size(312, 27);
            this.lbl_passwordmatch.TabIndex = 161;
            this.lbl_passwordmatch.Text = "Failed .. passwords don\'t match";
            this.lbl_passwordmatch.Visible = false;
            // 
            // btn_panelAddUserSave
            // 
            this.btn_panelAddUserSave.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_panelAddUserSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_panelAddUserSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_panelAddUserSave.BorderRadius = 0;
            this.btn_panelAddUserSave.ButtonText = "Save";
            this.btn_panelAddUserSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_panelAddUserSave, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_panelAddUserSave, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_panelAddUserSave, BunifuAnimatorNS.DecorationType.None);
            this.btn_panelAddUserSave.DisabledColor = System.Drawing.Color.Gray;
            this.btn_panelAddUserSave.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_panelAddUserSave.Iconimage = null;
            this.btn_panelAddUserSave.Iconimage_right = null;
            this.btn_panelAddUserSave.Iconimage_right_Selected = null;
            this.btn_panelAddUserSave.Iconimage_Selected = null;
            this.btn_panelAddUserSave.IconMarginLeft = 0;
            this.btn_panelAddUserSave.IconMarginRight = 0;
            this.btn_panelAddUserSave.IconRightVisible = false;
            this.btn_panelAddUserSave.IconRightZoom = 0D;
            this.btn_panelAddUserSave.IconVisible = false;
            this.btn_panelAddUserSave.IconZoom = 90D;
            this.btn_panelAddUserSave.IsTab = false;
            this.btn_panelAddUserSave.Location = new System.Drawing.Point(599, 290);
            this.btn_panelAddUserSave.Margin = new System.Windows.Forms.Padding(9, 14, 9, 14);
            this.btn_panelAddUserSave.Name = "btn_panelAddUserSave";
            this.btn_panelAddUserSave.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_panelAddUserSave.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btn_panelAddUserSave.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_panelAddUserSave.selected = false;
            this.btn_panelAddUserSave.Size = new System.Drawing.Size(356, 48);
            this.btn_panelAddUserSave.TabIndex = 160;
            this.btn_panelAddUserSave.Text = "Save";
            this.btn_panelAddUserSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_panelAddUserSave.Textcolor = System.Drawing.Color.White;
            this.btn_panelAddUserSave.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_panelAddUserSave.Click += new System.EventHandler(this.btn_panelAddUserSave_Click);
            // 
            // txt_newTraderId
            // 
            this.txt_newTraderId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderId, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderId, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderId, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderId.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderId.Location = new System.Drawing.Point(192, 223);
            this.txt_newTraderId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderId.Name = "txt_newTraderId";
            this.txt_newTraderId.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderId.TabIndex = 157;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.label14.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(37, 228);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 31);
            this.label14.TabIndex = 158;
            this.label14.Text = "用户代码";
            // 
            // txt_newTraderType
            // 
            this.txt_newTraderType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderType, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderType, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderType, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderType.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderType.Location = new System.Drawing.Point(727, 121);
            this.txt_newTraderType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderType.Name = "txt_newTraderType";
            this.txt_newTraderType.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderType.TabIndex = 155;
            this.txt_newTraderType.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.label15.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(503, 121);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 31);
            this.label15.TabIndex = 156;
            this.label15.Text = "用户类型";
            // 
            // txt_newTraderPass2
            // 
            this.txt_newTraderPass2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderPass2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderPass2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderPass2, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderPass2.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderPass2.Location = new System.Drawing.Point(727, 67);
            this.txt_newTraderPass2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderPass2.Name = "txt_newTraderPass2";
            this.txt_newTraderPass2.PasswordChar = '*';
            this.txt_newTraderPass2.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderPass2.TabIndex = 153;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.label19.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(503, 67);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(206, 31);
            this.label19.TabIndex = 154;
            this.label19.Text = "密码（重复校验）";
            // 
            // txt_newTraderPass
            // 
            this.txt_newTraderPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderPass, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderPass, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderPass, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderPass.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderPass.Location = new System.Drawing.Point(727, 18);
            this.txt_newTraderPass.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderPass.Name = "txt_newTraderPass";
            this.txt_newTraderPass.PasswordChar = '*';
            this.txt_newTraderPass.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderPass.TabIndex = 151;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.label21.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(503, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 31);
            this.label21.TabIndex = 152;
            this.label21.Text = "密码";
            // 
            // txt_newTraderBroker
            // 
            this.txt_newTraderBroker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderBroker, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderBroker, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderBroker, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderBroker.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderBroker.Location = new System.Drawing.Point(192, 167);
            this.txt_newTraderBroker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderBroker.Name = "txt_newTraderBroker";
            this.txt_newTraderBroker.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderBroker.TabIndex = 149;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.label10.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(31, 172);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(158, 31);
            this.label10.TabIndex = 150;
            this.label10.Text = "经纪公司编号";
            // 
            // txt_newTraderDept
            // 
            this.txt_newTraderDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderDept, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderDept, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderDept, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderDept.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderDept.Location = new System.Drawing.Point(192, 121);
            this.txt_newTraderDept.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderDept.Name = "txt_newTraderDept";
            this.txt_newTraderDept.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderDept.TabIndex = 147;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.label11.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(31, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 31);
            this.label11.TabIndex = 148;
            this.label11.Text = "营业部";
            // 
            // txt_newTraderName
            // 
            this.txt_newTraderName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderName, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderName, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderName, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderName.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderName.Location = new System.Drawing.Point(192, 67);
            this.txt_newTraderName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderName.Name = "txt_newTraderName";
            this.txt_newTraderName.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderName.TabIndex = 145;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.label12.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(31, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 31);
            this.label12.TabIndex = 146;
            this.label12.Text = "用户名称";
            // 
            // txt_newTraderSystemNum
            // 
            this.txt_newTraderSystemNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_newTraderSystemNum, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_newTraderSystemNum, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_newTraderSystemNum, BunifuAnimatorNS.DecorationType.None);
            this.txt_newTraderSystemNum.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_newTraderSystemNum.Location = new System.Drawing.Point(192, 18);
            this.txt_newTraderSystemNum.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_newTraderSystemNum.Name = "txt_newTraderSystemNum";
            this.txt_newTraderSystemNum.Size = new System.Drawing.Size(228, 36);
            this.txt_newTraderSystemNum.TabIndex = 143;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.label13.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(31, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 31);
            this.label13.TabIndex = 144;
            this.label13.Text = "系统编号";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label24, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label24, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label24, BunifuAnimatorNS.DecorationType.None);
            this.label24.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(13, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(134, 31);
            this.label24.TabIndex = 3;
            this.label24.Text = "新增交易员";
            // 
            // dg_users
            // 
            this.dg_users.AllowUserToAddRows = false;
            this.dg_users.AllowUserToDeleteRows = false;
            this.dg_users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_users, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_users, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_users, BunifuAnimatorNS.DecorationType.None);
            this.dg_users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_users.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dg_users.Location = new System.Drawing.Point(0, 0);
            this.dg_users.Name = "dg_users";
            this.dg_users.RowTemplate.Height = 31;
            this.dg_users.Size = new System.Drawing.Size(2053, 829);
            this.dg_users.TabIndex = 0;
            this.dg_users.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_users_CellContentClick);
            // 
            // pnl_user
            // 
            this.pnl_user.Controls.Add(this.label16);
            this.pnl_user.Controls.Add(this.btn_user_collapse);
            this.pnl_user.Controls.Add(this.pnl_user_Search2);
            this.panelAnimator.SetDecoration(this.pnl_user, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_user, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_user, BunifuAnimatorNS.DecorationType.None);
            this.pnl_user.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_user.Location = new System.Drawing.Point(4, 4);
            this.pnl_user.Name = "pnl_user";
            this.pnl_user.Size = new System.Drawing.Size(58, 829);
            this.pnl_user.TabIndex = 0;
            this.pnl_user.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_user_Paint);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.label16.Location = new System.Drawing.Point(156, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 27);
            this.label16.TabIndex = 131;
            this.label16.Text = "交易员查询";
            // 
            // btn_user_collapse
            // 
            this.btn_user_collapse.BackColor = System.Drawing.Color.Gainsboro;
            this.bunifuTransition1.SetDecoration(this.btn_user_collapse, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_user_collapse, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_user_collapse, BunifuAnimatorNS.DecorationType.None);
            this.btn_user_collapse.FlatAppearance.BorderSize = 0;
            this.btn_user_collapse.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_user_collapse.Location = new System.Drawing.Point(2, 2);
            this.btn_user_collapse.Margin = new System.Windows.Forms.Padding(0);
            this.btn_user_collapse.Name = "btn_user_collapse";
            this.btn_user_collapse.Size = new System.Drawing.Size(50, 58);
            this.btn_user_collapse.TabIndex = 128;
            this.btn_user_collapse.Text = ">>";
            this.btn_user_collapse.UseVisualStyleBackColor = false;
            this.btn_user_collapse.Click += new System.EventHandler(this.button6_Click);
            // 
            // pnl_user_Search2
            // 
            this.pnl_user_Search2.Controls.Add(this.bunifuFlatButton15);
            this.pnl_user_Search2.Controls.Add(this.txt_filter_traderId);
            this.pnl_user_Search2.Controls.Add(this.label20);
            this.pnl_user_Search2.Controls.Add(this.txt_filter_brokerNum);
            this.pnl_user_Search2.Controls.Add(this.label18);
            this.pnl_user_Search2.Controls.Add(this.txt_filter_sysNum);
            this.pnl_user_Search2.Controls.Add(this.label17);
            this.panelAnimator.SetDecoration(this.pnl_user_Search2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_user_Search2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_user_Search2, BunifuAnimatorNS.DecorationType.None);
            this.pnl_user_Search2.Location = new System.Drawing.Point(3, 63);
            this.pnl_user_Search2.Name = "pnl_user_Search2";
            this.pnl_user_Search2.Size = new System.Drawing.Size(432, 692);
            this.pnl_user_Search2.TabIndex = 130;
            this.pnl_user_Search2.Visible = false;
            // 
            // bunifuFlatButton15
            // 
            this.bunifuFlatButton15.Activecolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton15.BackColor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton15.BorderRadius = 0;
            this.bunifuFlatButton15.ButtonText = "增加";
            this.bunifuFlatButton15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton15, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton15, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton15.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton15.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton15.Iconimage = null;
            this.bunifuFlatButton15.Iconimage_right = null;
            this.bunifuFlatButton15.Iconimage_right_Selected = null;
            this.bunifuFlatButton15.Iconimage_Selected = null;
            this.bunifuFlatButton15.IconMarginLeft = 0;
            this.bunifuFlatButton15.IconMarginRight = 0;
            this.bunifuFlatButton15.IconRightVisible = false;
            this.bunifuFlatButton15.IconRightZoom = 0D;
            this.bunifuFlatButton15.IconVisible = false;
            this.bunifuFlatButton15.IconZoom = 90D;
            this.bunifuFlatButton15.IsTab = false;
            this.bunifuFlatButton15.Location = new System.Drawing.Point(19, 158);
            this.bunifuFlatButton15.Margin = new System.Windows.Forms.Padding(12, 21, 12, 21);
            this.bunifuFlatButton15.Name = "bunifuFlatButton15";
            this.bunifuFlatButton15.Normalcolor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton15.OnHovercolor = System.Drawing.Color.SkyBlue;
            this.bunifuFlatButton15.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton15.selected = false;
            this.bunifuFlatButton15.Size = new System.Drawing.Size(399, 44);
            this.bunifuFlatButton15.TabIndex = 138;
            this.bunifuFlatButton15.Text = "增加";
            this.bunifuFlatButton15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton15.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton15.TextFont = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton15.Click += new System.EventHandler(this.bunifuFlatButton15_Click_1);
            // 
            // txt_filter_traderId
            // 
            this.txt_filter_traderId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filter_traderId, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filter_traderId, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filter_traderId, BunifuAnimatorNS.DecorationType.None);
            this.txt_filter_traderId.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filter_traderId.Location = new System.Drawing.Point(189, 109);
            this.txt_filter_traderId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filter_traderId.Name = "txt_filter_traderId";
            this.txt_filter_traderId.Size = new System.Drawing.Size(228, 36);
            this.txt_filter_traderId.TabIndex = 132;
            this.txt_filter_traderId.TextChanged += new System.EventHandler(this.txt_filter_traderId_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.label20.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 31);
            this.label20.TabIndex = 133;
            this.label20.Text = "用户代码";
            // 
            // txt_filter_brokerNum
            // 
            this.txt_filter_brokerNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filter_brokerNum, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filter_brokerNum, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filter_brokerNum, BunifuAnimatorNS.DecorationType.None);
            this.txt_filter_brokerNum.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filter_brokerNum.Location = new System.Drawing.Point(189, 55);
            this.txt_filter_brokerNum.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filter_brokerNum.Name = "txt_filter_brokerNum";
            this.txt_filter_brokerNum.Size = new System.Drawing.Size(228, 36);
            this.txt_filter_brokerNum.TabIndex = 130;
            this.txt_filter_brokerNum.TextChanged += new System.EventHandler(this.txt_filter_brokerNum_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.label18.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(22, 60);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(158, 31);
            this.label18.TabIndex = 131;
            this.label18.Text = "经纪公司编号";
            // 
            // txt_filter_sysNum
            // 
            this.txt_filter_sysNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filter_sysNum, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filter_sysNum, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filter_sysNum, BunifuAnimatorNS.DecorationType.None);
            this.txt_filter_sysNum.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filter_sysNum.Location = new System.Drawing.Point(189, 6);
            this.txt_filter_sysNum.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filter_sysNum.Name = "txt_filter_sysNum";
            this.txt_filter_sysNum.Size = new System.Drawing.Size(228, 36);
            this.txt_filter_sysNum.TabIndex = 128;
            this.txt_filter_sysNum.TextChanged += new System.EventHandler(this.txt_filter_sysNum_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.label17.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 31);
            this.label17.TabIndex = 129;
            this.label17.Text = "系统编号";
            // 
            // Investor
            // 
            this.Investor.Controls.Add(this.pnl_dg_investor);
            this.Investor.Controls.Add(this.pnl_investor);
            this.slide_panel.SetDecoration(this.Investor, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.Investor, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.Investor, BunifuAnimatorNS.DecorationType.None);
            this.Investor.Location = new System.Drawing.Point(4, 36);
            this.Investor.Name = "Investor";
            this.Investor.Padding = new System.Windows.Forms.Padding(3);
            this.Investor.Size = new System.Drawing.Size(2119, 837);
            this.Investor.TabIndex = 2;
            this.Investor.Text = "投资者信息管理";
            this.Investor.UseVisualStyleBackColor = true;
            // 
            // pnl_dg_investor
            // 
            this.pnl_dg_investor.Controls.Add(this.pnl_add_investor);
            this.pnl_dg_investor.Controls.Add(this.dg_InvestorDetails);
            this.panelAnimator.SetDecoration(this.pnl_dg_investor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_dg_investor, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_dg_investor, BunifuAnimatorNS.DecorationType.None);
            this.pnl_dg_investor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_dg_investor.Location = new System.Drawing.Point(60, 3);
            this.pnl_dg_investor.Name = "pnl_dg_investor";
            this.pnl_dg_investor.Size = new System.Drawing.Size(2056, 831);
            this.pnl_dg_investor.TabIndex = 12;
            // 
            // pnl_add_investor
            // 
            this.pnl_add_investor.BackColor = System.Drawing.Color.SteelBlue;
            this.pnl_add_investor.Controls.Add(this.btn_close_addInvestor);
            this.pnl_add_investor.Controls.Add(this.panel16);
            this.pnl_add_investor.Controls.Add(this.label63);
            this.panelAnimator.SetDecoration(this.pnl_add_investor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_add_investor, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_add_investor, BunifuAnimatorNS.DecorationType.None);
            this.pnl_add_investor.Location = new System.Drawing.Point(117, 225);
            this.pnl_add_investor.Name = "pnl_add_investor";
            this.pnl_add_investor.Size = new System.Drawing.Size(985, 284);
            this.pnl_add_investor.TabIndex = 12;
            this.pnl_add_investor.Visible = false;
            // 
            // btn_close_addInvestor
            // 
            this.btn_close_addInvestor.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.btn_close_addInvestor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_close_addInvestor, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_close_addInvestor, BunifuAnimatorNS.DecorationType.None);
            this.btn_close_addInvestor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close_addInvestor.ForeColor = System.Drawing.Color.White;
            this.btn_close_addInvestor.Location = new System.Drawing.Point(940, 7);
            this.btn_close_addInvestor.Margin = new System.Windows.Forms.Padding(4);
            this.btn_close_addInvestor.Name = "btn_close_addInvestor";
            this.btn_close_addInvestor.Size = new System.Drawing.Size(38, 38);
            this.btn_close_addInvestor.TabIndex = 145;
            this.btn_close_addInvestor.Text = "X";
            this.btn_close_addInvestor.UseVisualStyleBackColor = false;
            this.btn_close_addInvestor.Click += new System.EventHandler(this.btn_close_addInvestor_Click);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.White;
            this.panel16.Controls.Add(this.lbl_save_investor_error);
            this.panel16.Controls.Add(this.cmb_investor_exch_add);
            this.panel16.Controls.Add(this.txt_investor_mm_add);
            this.panel16.Controls.Add(this.label33);
            this.panel16.Controls.Add(this.txt_investor_hedge_add);
            this.panel16.Controls.Add(this.label36);
            this.panel16.Controls.Add(this.txt_investor_arbitrage_add);
            this.panel16.Controls.Add(this.label37);
            this.panel16.Controls.Add(this.txt_investor_speculate_add);
            this.panel16.Controls.Add(this.label38);
            this.panel16.Controls.Add(this.btn_investor_add);
            this.panel16.Controls.Add(this.label61);
            this.panel16.Controls.Add(this.txt_investor_account_add);
            this.panel16.Controls.Add(this.label62);
            this.panelAnimator.SetDecoration(this.panel16, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel16, BunifuAnimatorNS.DecorationType.None);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(0, 54);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(985, 230);
            this.panel16.TabIndex = 144;
            // 
            // lbl_save_investor_error
            // 
            this.lbl_save_investor_error.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.lbl_save_investor_error, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.lbl_save_investor_error, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.lbl_save_investor_error, BunifuAnimatorNS.DecorationType.None);
            this.lbl_save_investor_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_save_investor_error.Location = new System.Drawing.Point(362, 182);
            this.lbl_save_investor_error.Name = "lbl_save_investor_error";
            this.lbl_save_investor_error.Size = new System.Drawing.Size(152, 27);
            this.lbl_save_investor_error.TabIndex = 171;
            this.lbl_save_investor_error.Text = "代码必须是数字";
            this.lbl_save_investor_error.Visible = false;
            // 
            // cmb_investor_exch_add
            // 
            this.slide_panel.SetDecoration(this.cmb_investor_exch_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.cmb_investor_exch_add, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.cmb_investor_exch_add, BunifuAnimatorNS.DecorationType.None);
            this.cmb_investor_exch_add.FormattingEnabled = true;
            this.cmb_investor_exch_add.Items.AddRange(new object[] {
            "SHFE",
            "DCE",
            "CZCE",
            "CFFEX",
            "INE"});
            this.cmb_investor_exch_add.Location = new System.Drawing.Point(191, 69);
            this.cmb_investor_exch_add.Name = "cmb_investor_exch_add";
            this.cmb_investor_exch_add.Size = new System.Drawing.Size(229, 35);
            this.cmb_investor_exch_add.TabIndex = 170;
            // 
            // txt_investor_mm_add
            // 
            this.txt_investor_mm_add.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_mm_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_mm_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_mm_add, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_mm_add.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_mm_add.Location = new System.Drawing.Point(730, 116);
            this.txt_investor_mm_add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_mm_add.Name = "txt_investor_mm_add";
            this.txt_investor_mm_add.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_mm_add.TabIndex = 168;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label33, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label33, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label33, BunifuAnimatorNS.DecorationType.None);
            this.label33.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(563, 121);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(134, 31);
            this.label33.TabIndex = 169;
            this.label33.Text = "做市商代码";
            // 
            // txt_investor_hedge_add
            // 
            this.txt_investor_hedge_add.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_hedge_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_hedge_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_hedge_add, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_hedge_add.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_hedge_add.Location = new System.Drawing.Point(730, 67);
            this.txt_investor_hedge_add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_hedge_add.Name = "txt_investor_hedge_add";
            this.txt_investor_hedge_add.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_hedge_add.TabIndex = 166;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label36, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label36, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label36, BunifuAnimatorNS.DecorationType.None);
            this.label36.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(563, 72);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(110, 31);
            this.label36.TabIndex = 167;
            this.label36.Text = "套保代码";
            // 
            // txt_investor_arbitrage_add
            // 
            this.txt_investor_arbitrage_add.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_arbitrage_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_arbitrage_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_arbitrage_add, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_arbitrage_add.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_arbitrage_add.Location = new System.Drawing.Point(730, 18);
            this.txt_investor_arbitrage_add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_arbitrage_add.Name = "txt_investor_arbitrage_add";
            this.txt_investor_arbitrage_add.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_arbitrage_add.TabIndex = 164;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label37, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label37, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label37, BunifuAnimatorNS.DecorationType.None);
            this.label37.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(563, 23);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 31);
            this.label37.TabIndex = 165;
            this.label37.Text = "套利代码";
            // 
            // txt_investor_speculate_add
            // 
            this.txt_investor_speculate_add.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_speculate_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_speculate_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_speculate_add, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_speculate_add.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_speculate_add.Location = new System.Drawing.Point(191, 116);
            this.txt_investor_speculate_add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_speculate_add.Name = "txt_investor_speculate_add";
            this.txt_investor_speculate_add.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_speculate_add.TabIndex = 162;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label38, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label38, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label38, BunifuAnimatorNS.DecorationType.None);
            this.label38.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(24, 121);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(110, 31);
            this.label38.TabIndex = 163;
            this.label38.Text = "投机代码";
            // 
            // btn_investor_add
            // 
            this.btn_investor_add.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_investor_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_investor_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_investor_add.BorderRadius = 0;
            this.btn_investor_add.ButtonText = "Save";
            this.btn_investor_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.btn_investor_add.DisabledColor = System.Drawing.Color.Gray;
            this.btn_investor_add.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_investor_add.Iconimage = null;
            this.btn_investor_add.Iconimage_right = null;
            this.btn_investor_add.Iconimage_right_Selected = null;
            this.btn_investor_add.Iconimage_Selected = null;
            this.btn_investor_add.IconMarginLeft = 0;
            this.btn_investor_add.IconMarginRight = 0;
            this.btn_investor_add.IconRightVisible = false;
            this.btn_investor_add.IconRightZoom = 0D;
            this.btn_investor_add.IconVisible = false;
            this.btn_investor_add.IconZoom = 90D;
            this.btn_investor_add.IsTab = false;
            this.btn_investor_add.Location = new System.Drawing.Point(602, 170);
            this.btn_investor_add.Margin = new System.Windows.Forms.Padding(9, 14, 9, 14);
            this.btn_investor_add.Name = "btn_investor_add";
            this.btn_investor_add.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_investor_add.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btn_investor_add.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_investor_add.selected = false;
            this.btn_investor_add.Size = new System.Drawing.Size(356, 48);
            this.btn_investor_add.TabIndex = 160;
            this.btn_investor_add.Text = "Save";
            this.btn_investor_add.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_investor_add.Textcolor = System.Drawing.Color.White;
            this.btn_investor_add.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_investor_add.Click += new System.EventHandler(this.btn_investor_add_Click_1);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label61, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label61, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label61, BunifuAnimatorNS.DecorationType.None);
            this.label61.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(31, 72);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(86, 31);
            this.label61.TabIndex = 146;
            this.label61.Text = "交易所";
            // 
            // txt_investor_account_add
            // 
            this.txt_investor_account_add.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_account_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_account_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_account_add, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_account_add.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_account_add.Location = new System.Drawing.Point(192, 18);
            this.txt_investor_account_add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_account_add.Name = "txt_investor_account_add";
            this.txt_investor_account_add.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_account_add.TabIndex = 143;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label62, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label62, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label62, BunifuAnimatorNS.DecorationType.None);
            this.label62.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(31, 23);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(110, 31);
            this.label62.TabIndex = 144;
            this.label62.Text = "客户代码";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label63, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label63, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label63, BunifuAnimatorNS.DecorationType.None);
            this.label63.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.White;
            this.label63.Location = new System.Drawing.Point(13, 10);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(134, 31);
            this.label63.TabIndex = 3;
            this.label63.Text = "新增投资者";
            // 
            // dg_InvestorDetails
            // 
            this.dg_InvestorDetails.AllowUserToAddRows = false;
            this.dg_InvestorDetails.AllowUserToDeleteRows = false;
            this.dg_InvestorDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_InvestorDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_InvestorDetails.ContextMenuStrip = this.ctx_investor;
            this.slide_panel.SetDecoration(this.dg_InvestorDetails, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_InvestorDetails, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_InvestorDetails, BunifuAnimatorNS.DecorationType.None);
            this.dg_InvestorDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_InvestorDetails.Location = new System.Drawing.Point(0, 0);
            this.dg_InvestorDetails.Name = "dg_InvestorDetails";
            this.dg_InvestorDetails.RowTemplate.Height = 31;
            this.dg_InvestorDetails.Size = new System.Drawing.Size(2056, 831);
            this.dg_InvestorDetails.TabIndex = 8;
            this.dg_InvestorDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_InvestorDetails_CellEndEdit);
            // 
            // ctx_investor
            // 
            this.bunifuTransition1.SetDecoration(this.ctx_investor, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.ctx_investor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.ctx_investor, BunifuAnimatorNS.DecorationType.None);
            this.ctx_investor.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ctx_investor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.ctx_investor.Name = "ctx_investor";
            this.ctx_investor.Size = new System.Drawing.Size(174, 32);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(173, 28);
            this.toolStripMenuItem2.Text = "DeleteRow";
            // 
            // pnl_investor
            // 
            this.pnl_investor.Controls.Add(this.pnl_investor_add);
            this.pnl_investor.Controls.Add(this.btnExpInvestor);
            this.panelAnimator.SetDecoration(this.pnl_investor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_investor, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_investor, BunifuAnimatorNS.DecorationType.None);
            this.pnl_investor.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_investor.Location = new System.Drawing.Point(3, 3);
            this.pnl_investor.Name = "pnl_investor";
            this.pnl_investor.Size = new System.Drawing.Size(57, 831);
            this.pnl_investor.TabIndex = 2;
            // 
            // pnl_investor_add
            // 
            this.pnl_investor_add.Controls.Add(this.txt_investor_exch);
            this.pnl_investor_add.Controls.Add(this.btn_investor_open_add);
            this.pnl_investor_add.Controls.Add(this.label26);
            this.pnl_investor_add.Controls.Add(this.txt_investor_account);
            this.pnl_investor_add.Controls.Add(this.label27);
            this.panelAnimator.SetDecoration(this.pnl_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.pnl_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pnl_investor_add, BunifuAnimatorNS.DecorationType.None);
            this.pnl_investor_add.Location = new System.Drawing.Point(3, 63);
            this.pnl_investor_add.Name = "pnl_investor_add";
            this.pnl_investor_add.Size = new System.Drawing.Size(438, 692);
            this.pnl_investor_add.TabIndex = 131;
            this.pnl_investor_add.Visible = false;
            // 
            // txt_investor_exch
            // 
            this.slide_panel.SetDecoration(this.txt_investor_exch, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_exch, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.txt_investor_exch, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_exch.FormattingEnabled = true;
            this.txt_investor_exch.Items.AddRange(new object[] {
            "SHFE",
            "DCE",
            "CZCE",
            "CFFEX",
            "INE"});
            this.txt_investor_exch.Location = new System.Drawing.Point(189, 55);
            this.txt_investor_exch.Name = "txt_investor_exch";
            this.txt_investor_exch.Size = new System.Drawing.Size(229, 35);
            this.txt_investor_exch.TabIndex = 143;
            this.txt_investor_exch.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // btn_investor_open_add
            // 
            this.btn_investor_open_add.Activecolor = System.Drawing.Color.SteelBlue;
            this.btn_investor_open_add.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_investor_open_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_investor_open_add.BorderRadius = 0;
            this.btn_investor_open_add.ButtonText = "增加";
            this.btn_investor_open_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_investor_open_add, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_investor_open_add, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_investor_open_add, BunifuAnimatorNS.DecorationType.None);
            this.btn_investor_open_add.DisabledColor = System.Drawing.Color.Gray;
            this.btn_investor_open_add.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_investor_open_add.Iconimage = null;
            this.btn_investor_open_add.Iconimage_right = null;
            this.btn_investor_open_add.Iconimage_right_Selected = null;
            this.btn_investor_open_add.Iconimage_Selected = null;
            this.btn_investor_open_add.IconMarginLeft = 0;
            this.btn_investor_open_add.IconMarginRight = 0;
            this.btn_investor_open_add.IconRightVisible = false;
            this.btn_investor_open_add.IconRightZoom = 0D;
            this.btn_investor_open_add.IconVisible = false;
            this.btn_investor_open_add.IconZoom = 90D;
            this.btn_investor_open_add.IsTab = false;
            this.btn_investor_open_add.Location = new System.Drawing.Point(18, 109);
            this.btn_investor_open_add.Margin = new System.Windows.Forms.Padding(12, 21, 12, 21);
            this.btn_investor_open_add.Name = "btn_investor_open_add";
            this.btn_investor_open_add.Normalcolor = System.Drawing.Color.LightSkyBlue;
            this.btn_investor_open_add.OnHovercolor = System.Drawing.Color.SkyBlue;
            this.btn_investor_open_add.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_investor_open_add.selected = false;
            this.btn_investor_open_add.Size = new System.Drawing.Size(399, 38);
            this.btn_investor_open_add.TabIndex = 138;
            this.btn_investor_open_add.Text = "增加";
            this.btn_investor_open_add.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_investor_open_add.Textcolor = System.Drawing.Color.White;
            this.btn_investor_open_add.TextFont = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_investor_open_add.Click += new System.EventHandler(this.btn_investor_add_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label26, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label26, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label26, BunifuAnimatorNS.DecorationType.None);
            this.label26.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(22, 60);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 31);
            this.label26.TabIndex = 131;
            this.label26.Text = "交易所";
            // 
            // txt_investor_account
            // 
            this.txt_investor_account.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_investor_account, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_investor_account, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_investor_account, BunifuAnimatorNS.DecorationType.None);
            this.txt_investor_account.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_investor_account.Location = new System.Drawing.Point(189, 6);
            this.txt_investor_account.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_investor_account.Name = "txt_investor_account";
            this.txt_investor_account.Size = new System.Drawing.Size(228, 36);
            this.txt_investor_account.TabIndex = 128;
            this.txt_investor_account.TextChanged += new System.EventHandler(this.txt_filter_investor_account_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label27, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label27, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label27, BunifuAnimatorNS.DecorationType.None);
            this.label27.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 31);
            this.label27.TabIndex = 129;
            this.label27.Text = "客户代码";
            // 
            // btnExpInvestor
            // 
            this.bunifuTransition1.SetDecoration(this.btnExpInvestor, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btnExpInvestor, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btnExpInvestor, BunifuAnimatorNS.DecorationType.None);
            this.btnExpInvestor.Location = new System.Drawing.Point(2, 2);
            this.btnExpInvestor.Name = "btnExpInvestor";
            this.btnExpInvestor.Size = new System.Drawing.Size(50, 58);
            this.btnExpInvestor.TabIndex = 127;
            this.btnExpInvestor.Text = ">>";
            this.btnExpInvestor.UseVisualStyleBackColor = true;
            this.btnExpInvestor.Click += new System.EventHandler(this.btnExpInvestor_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dg_AccountMap);
            this.tabPage4.Controls.Add(this.panel15);
            this.slide_panel.SetDecoration(this.tabPage4, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tabPage4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tabPage4, BunifuAnimatorNS.DecorationType.None);
            this.tabPage4.Location = new System.Drawing.Point(4, 36);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(2119, 837);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "用户投资者关系管理";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dg_AccountMap
            // 
            this.dg_AccountMap.AllowUserToAddRows = false;
            this.dg_AccountMap.AllowUserToDeleteRows = false;
            this.dg_AccountMap.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_AccountMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_AccountMap.ContextMenuStrip = this.contextMenuStrip1;
            this.slide_panel.SetDecoration(this.dg_AccountMap, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_AccountMap, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_AccountMap, BunifuAnimatorNS.DecorationType.None);
            this.dg_AccountMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.dg_AccountMap.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dg_AccountMap.Location = new System.Drawing.Point(3, 144);
            this.dg_AccountMap.Name = "dg_AccountMap";
            this.dg_AccountMap.ReadOnly = true;
            this.dg_AccountMap.RowTemplate.Height = 31;
            this.dg_AccountMap.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_AccountMap.Size = new System.Drawing.Size(2113, 834);
            this.dg_AccountMap.TabIndex = 8;
            this.dg_AccountMap.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_AccountMap_CellMouseDown);
            // 
            // contextMenuStrip1
            // 
            this.bunifuTransition1.SetDecoration(this.contextMenuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.contextMenuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.contextMenuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteRow});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(174, 34);
            this.contextMenuStrip1.Click += new System.EventHandler(this.contextMenuStrip1_Click);
            // 
            // DeleteRow
            // 
            this.DeleteRow.AutoSize = false;
            this.DeleteRow.Name = "DeleteRow";
            this.DeleteRow.Size = new System.Drawing.Size(120, 30);
            this.DeleteRow.Text = "DeleteRow";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label29);
            this.panel15.Controls.Add(this.btn_addAccountMap);
            this.panel15.Controls.Add(this.cmb_account);
            this.panel15.Controls.Add(this.cmb_username);
            this.panel15.Controls.Add(this.bunifuFlatButton11);
            this.panel15.Controls.Add(this.bunifuFlatButton12);
            this.panel15.Controls.Add(this.label31);
            this.panel15.Controls.Add(this.label32);
            this.panelAnimator.SetDecoration(this.panel15, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel15, BunifuAnimatorNS.DecorationType.None);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(3, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(2113, 141);
            this.panel15.TabIndex = 3;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label29, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label29, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label29, BunifuAnimatorNS.DecorationType.None);
            this.label29.ForeColor = System.Drawing.Color.DimGray;
            this.label29.Location = new System.Drawing.Point(785, 74);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 27);
            this.label29.TabIndex = 128;
            this.label29.Text = "← → ";
            // 
            // btn_addAccountMap
            // 
            this.btn_addAccountMap.Activecolor = System.Drawing.SystemColors.ControlDark;
            this.btn_addAccountMap.BackColor = System.Drawing.Color.DarkGray;
            this.btn_addAccountMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_addAccountMap.BorderRadius = 0;
            this.btn_addAccountMap.ButtonText = "新增关联";
            this.btn_addAccountMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.btn_addAccountMap, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.btn_addAccountMap, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.btn_addAccountMap, BunifuAnimatorNS.DecorationType.None);
            this.btn_addAccountMap.DisabledColor = System.Drawing.Color.Gray;
            this.btn_addAccountMap.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_addAccountMap.Iconimage = null;
            this.btn_addAccountMap.Iconimage_right = null;
            this.btn_addAccountMap.Iconimage_right_Selected = null;
            this.btn_addAccountMap.Iconimage_Selected = null;
            this.btn_addAccountMap.IconMarginLeft = 0;
            this.btn_addAccountMap.IconMarginRight = 0;
            this.btn_addAccountMap.IconRightVisible = false;
            this.btn_addAccountMap.IconRightZoom = 0D;
            this.btn_addAccountMap.IconVisible = false;
            this.btn_addAccountMap.IconZoom = 90D;
            this.btn_addAccountMap.IsTab = false;
            this.btn_addAccountMap.Location = new System.Drawing.Point(1240, 23);
            this.btn_addAccountMap.Margin = new System.Windows.Forms.Padding(9, 14, 9, 14);
            this.btn_addAccountMap.Name = "btn_addAccountMap";
            this.btn_addAccountMap.Normalcolor = System.Drawing.Color.DarkGray;
            this.btn_addAccountMap.OnHovercolor = System.Drawing.Color.Gray;
            this.btn_addAccountMap.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_addAccountMap.selected = false;
            this.btn_addAccountMap.Size = new System.Drawing.Size(137, 83);
            this.btn_addAccountMap.TabIndex = 127;
            this.btn_addAccountMap.Text = "新增关联";
            this.btn_addAccountMap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_addAccountMap.Textcolor = System.Drawing.Color.White;
            this.btn_addAccountMap.TextFont = new System.Drawing.Font("Microsoft YaHei", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_addAccountMap.Click += new System.EventHandler(this.btn_addAccountMap_Click);
            // 
            // cmb_account
            // 
            this.slide_panel.SetDecoration(this.cmb_account, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.cmb_account, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.cmb_account, BunifuAnimatorNS.DecorationType.None);
            this.cmb_account.FormattingEnabled = true;
            this.cmb_account.Location = new System.Drawing.Point(877, 66);
            this.cmb_account.Name = "cmb_account";
            this.cmb_account.Size = new System.Drawing.Size(276, 35);
            this.cmb_account.TabIndex = 127;
            // 
            // cmb_username
            // 
            this.slide_panel.SetDecoration(this.cmb_username, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.cmb_username, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.cmb_username, BunifuAnimatorNS.DecorationType.None);
            this.cmb_username.FormattingEnabled = true;
            this.cmb_username.Location = new System.Drawing.Point(474, 71);
            this.cmb_username.Name = "cmb_username";
            this.cmb_username.Size = new System.Drawing.Size(276, 35);
            this.cmb_username.TabIndex = 126;
            // 
            // bunifuFlatButton11
            // 
            this.bunifuFlatButton11.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton11.BorderRadius = 0;
            this.bunifuFlatButton11.ButtonText = "增加";
            this.bunifuFlatButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton11.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton11.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.Iconimage = null;
            this.bunifuFlatButton11.Iconimage_right = null;
            this.bunifuFlatButton11.Iconimage_right_Selected = null;
            this.bunifuFlatButton11.Iconimage_Selected = null;
            this.bunifuFlatButton11.IconMarginLeft = 0;
            this.bunifuFlatButton11.IconMarginRight = 0;
            this.bunifuFlatButton11.IconRightVisible = false;
            this.bunifuFlatButton11.IconRightZoom = 0D;
            this.bunifuFlatButton11.IconVisible = false;
            this.bunifuFlatButton11.IconZoom = 90D;
            this.bunifuFlatButton11.IsTab = false;
            this.bunifuFlatButton11.Location = new System.Drawing.Point(188, 272);
            this.bunifuFlatButton11.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton11.Name = "bunifuFlatButton11";
            this.bunifuFlatButton11.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton11.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuFlatButton11.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton11.selected = false;
            this.bunifuFlatButton11.Size = new System.Drawing.Size(213, 52);
            this.bunifuFlatButton11.TabIndex = 125;
            this.bunifuFlatButton11.Text = "增加";
            this.bunifuFlatButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton11.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton11.TextFont = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton12
            // 
            this.bunifuFlatButton12.Activecolor = System.Drawing.Color.LightCoral;
            this.bunifuFlatButton12.BackColor = System.Drawing.Color.LightCoral;
            this.bunifuFlatButton12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton12.BorderRadius = 0;
            this.bunifuFlatButton12.ButtonText = "";
            this.bunifuFlatButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton12.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton12.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton12.Iconimage = null;
            this.bunifuFlatButton12.Iconimage_right = null;
            this.bunifuFlatButton12.Iconimage_right_Selected = null;
            this.bunifuFlatButton12.Iconimage_Selected = null;
            this.bunifuFlatButton12.IconMarginLeft = 0;
            this.bunifuFlatButton12.IconMarginRight = 0;
            this.bunifuFlatButton12.IconRightVisible = false;
            this.bunifuFlatButton12.IconRightZoom = 0D;
            this.bunifuFlatButton12.IconVisible = false;
            this.bunifuFlatButton12.IconZoom = 90D;
            this.bunifuFlatButton12.IsTab = false;
            this.bunifuFlatButton12.Location = new System.Drawing.Point(-3, 272);
            this.bunifuFlatButton12.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton12.Name = "bunifuFlatButton12";
            this.bunifuFlatButton12.Normalcolor = System.Drawing.Color.LightCoral;
            this.bunifuFlatButton12.OnHovercolor = System.Drawing.Color.DarkSalmon;
            this.bunifuFlatButton12.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton12.selected = false;
            this.bunifuFlatButton12.Size = new System.Drawing.Size(185, 52);
            this.bunifuFlatButton12.TabIndex = 124;
            this.bunifuFlatButton12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton12.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton12.TextFont = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label31, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label31, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label31, BunifuAnimatorNS.DecorationType.None);
            this.label31.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(970, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(110, 31);
            this.label31.TabIndex = 117;
            this.label31.Text = "资金账户";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label32, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label32, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label32, BunifuAnimatorNS.DecorationType.None);
            this.label32.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(566, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(110, 31);
            this.label32.TabIndex = 115;
            this.label32.Text = "客户代码";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel20);
            this.tabPage3.Controls.Add(this.dg_loginUsers);
            this.tabPage3.Controls.Add(this.panel8);
            this.slide_panel.SetDecoration(this.tabPage3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tabPage3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tabPage3, BunifuAnimatorNS.DecorationType.None);
            this.tabPage3.Location = new System.Drawing.Point(4, 36);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(2119, 837);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "登录用户管理";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.txt_filter_loginUser);
            this.panel20.Controls.Add(this.label56);
            this.panel20.Controls.Add(this.textBox2);
            this.panel20.Controls.Add(this.label57);
            this.panel20.Controls.Add(this.label58);
            this.panelAnimator.SetDecoration(this.panel20, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel20, BunifuAnimatorNS.DecorationType.None);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(3, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(2113, 63);
            this.panel20.TabIndex = 16;
            // 
            // txt_filter_loginUser
            // 
            this.txt_filter_loginUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filter_loginUser, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filter_loginUser, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filter_loginUser, BunifuAnimatorNS.DecorationType.None);
            this.txt_filter_loginUser.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filter_loginUser.Location = new System.Drawing.Point(1507, 14);
            this.txt_filter_loginUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filter_loginUser.Name = "txt_filter_loginUser";
            this.txt_filter_loginUser.Size = new System.Drawing.Size(228, 36);
            this.txt_filter_loginUser.TabIndex = 116;
            this.txt_filter_loginUser.TextChanged += new System.EventHandler(this.txtFilterUserLogin_TextChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label56, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label56, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label56, BunifuAnimatorNS.DecorationType.None);
            this.label56.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(1391, 19);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(110, 31);
            this.label56.TabIndex = 117;
            this.label56.Text = "客户代码";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox2, BunifuAnimatorNS.DecorationType.None);
            this.textBox2.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox2.Location = new System.Drawing.Point(1073, 14);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(228, 36);
            this.textBox2.TabIndex = 114;
            this.textBox2.Visible = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label57, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label57, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label57, BunifuAnimatorNS.DecorationType.None);
            this.label57.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(957, 19);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(110, 31);
            this.label57.TabIndex = 115;
            this.label57.Text = "客户代码";
            this.label57.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label58, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label58, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label58, BunifuAnimatorNS.DecorationType.None);
            this.label58.Location = new System.Drawing.Point(848, 23);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(74, 27);
            this.label58.TabIndex = 0;
            this.label58.Text = "FILTER";
            this.label58.Visible = false;
            // 
            // dg_loginUsers
            // 
            this.dg_loginUsers.AllowUserToAddRows = false;
            this.dg_loginUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_loginUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_loginUsers, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_loginUsers, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_loginUsers, BunifuAnimatorNS.DecorationType.None);
            this.dg_loginUsers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dg_loginUsers.Location = new System.Drawing.Point(3, 60);
            this.dg_loginUsers.Name = "dg_loginUsers";
            this.dg_loginUsers.RowTemplate.Height = 31;
            this.dg_loginUsers.Size = new System.Drawing.Size(2113, 774);
            this.dg_loginUsers.TabIndex = 9;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label4);
            this.panelAnimator.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.panel8.Location = new System.Drawing.Point(2044, 216);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(827, 312);
            this.panel8.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.label4.Location = new System.Drawing.Point(17, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 27);
            this.label4.TabIndex = 1;
            this.label4.Text = "持仓";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.textBox14);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.checkBox6);
            this.tabPage5.Controls.Add(this.checkBox5);
            this.tabPage5.Controls.Add(this.checkBox3);
            this.tabPage5.Controls.Add(this.checkBox2);
            this.tabPage5.Controls.Add(this.checkBox1);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.comboBox1);
            this.slide_panel.SetDecoration(this.tabPage5, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tabPage5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tabPage5, BunifuAnimatorNS.DecorationType.None);
            this.tabPage5.Location = new System.Drawing.Point(4, 36);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(2119, 837);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "数据导入";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.bunifuTransition1.SetDecoration(this.button4, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.button4, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.button4, BunifuAnimatorNS.DecorationType.None);
            this.button4.Location = new System.Drawing.Point(1179, 194);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 48);
            this.button4.TabIndex = 114;
            this.button4.Text = "确认导入";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.bunifuTransition1.SetDecoration(this.button3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.button3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.button3, BunifuAnimatorNS.DecorationType.None);
            this.button3.Location = new System.Drawing.Point(1061, 194);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(112, 48);
            this.button3.TabIndex = 113;
            this.button3.Text = "CLEAR";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.bunifuTransition1.SetDecoration(this.textBox14, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox14, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox14, BunifuAnimatorNS.DecorationType.None);
            this.textBox14.Location = new System.Drawing.Point(194, 194);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(860, 34);
            this.textBox14.TabIndex = 112;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label35, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label35, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label35, BunifuAnimatorNS.DecorationType.None);
            this.label35.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(44, 152);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(230, 31);
            this.label35.TabIndex = 111;
            this.label35.Text = "选择数据在的文件夹";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.checkBox6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.checkBox6, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.checkBox6, BunifuAnimatorNS.DecorationType.None);
            this.checkBox6.Location = new System.Drawing.Point(962, 96);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(218, 31);
            this.checkBox6.TabIndex = 110;
            this.checkBox6.Text = "导入未上市期权合约";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.checkBox5, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.checkBox5, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.checkBox5, BunifuAnimatorNS.DecorationType.None);
            this.checkBox5.Location = new System.Drawing.Point(689, 96);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(218, 31);
            this.checkBox5.TabIndex = 109;
            this.checkBox5.Text = "导入期权保证金文件";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.checkBox3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.checkBox3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.checkBox3, BunifuAnimatorNS.DecorationType.None);
            this.checkBox3.Location = new System.Drawing.Point(455, 96);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(198, 31);
            this.checkBox3.TabIndex = 107;
            this.checkBox3.Text = "导入期权持仓文件";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.checkBox2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.checkBox2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.checkBox2, BunifuAnimatorNS.DecorationType.None);
            this.checkBox2.Location = new System.Drawing.Point(250, 96);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(158, 31);
            this.checkBox2.TabIndex = 106;
            this.checkBox2.Text = "导入持仓文件";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.checkBox1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.checkBox1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.checkBox1, BunifuAnimatorNS.DecorationType.None);
            this.checkBox1.Location = new System.Drawing.Point(49, 96);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(158, 31);
            this.checkBox1.TabIndex = 105;
            this.checkBox1.Text = "导入资金文件";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label34, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label34, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label34, BunifuAnimatorNS.DecorationType.None);
            this.label34.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(43, 30);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 31);
            this.label34.TabIndex = 104;
            this.label34.Text = "客户代码";
            // 
            // comboBox1
            // 
            this.slide_panel.SetDecoration(this.comboBox1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.comboBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.comboBox1, BunifuAnimatorNS.DecorationType.None);
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox1.Font = new System.Drawing.Font("Roboto", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(167, 30);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(260, 37);
            this.comboBox1.TabIndex = 103;
            // 
            // TransferMoney
            // 
            this.slide_panel.SetDecoration(this.TransferMoney, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.TransferMoney, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.TransferMoney, BunifuAnimatorNS.DecorationType.None);
            this.TransferMoney.Location = new System.Drawing.Point(4, 36);
            this.TransferMoney.Name = "TransferMoney";
            this.TransferMoney.Size = new System.Drawing.Size(2119, 837);
            this.TransferMoney.TabIndex = 5;
            this.TransferMoney.Text = "出入金申请";
            this.TransferMoney.UseVisualStyleBackColor = true;
            // 
            // Account
            // 
            this.Account.Controls.Add(this.panel7);
            this.Account.Controls.Add(this.panel6);
            this.Account.Controls.Add(this.panel5);
            this.Account.Controls.Add(this.panel4);
            this.Account.Controls.Add(this.bunifuSwitch1);
            this.slide_panel.SetDecoration(this.Account, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.Account, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.Account, BunifuAnimatorNS.DecorationType.None);
            this.Account.Location = new System.Drawing.Point(4, 36);
            this.Account.Margin = new System.Windows.Forms.Padding(4);
            this.Account.Name = "Account";
            this.Account.Padding = new System.Windows.Forms.Padding(4);
            this.Account.Size = new System.Drawing.Size(2119, 837);
            this.Account.TabIndex = 0;
            this.Account.Text = "基金公司信息管理";
            this.Account.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Window;
            this.panel7.Controls.Add(this.dataGridView3);
            this.panel7.Controls.Add(this.bunifuFlatButton4);
            this.panel7.Controls.Add(this.bunifuFlatButton5);
            this.panel7.Controls.Add(this.bunifuFlatButton6);
            this.panel7.Controls.Add(this.bunifuCustomLabel6);
            this.panelAnimator.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(4, 494);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(2111, 286);
            this.panel7.TabIndex = 6;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dataGridView3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dataGridView3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dataGridView3, BunifuAnimatorNS.DecorationType.None);
            this.dataGridView3.Location = new System.Drawing.Point(50, 42);
            this.dataGridView3.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(1178, 216);
            this.dataGridView3.TabIndex = 10;
            // 
            // bunifuFlatButton4
            // 
            this.bunifuFlatButton4.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton4.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton4.BorderRadius = 1;
            this.bunifuFlatButton4.ButtonText = "重置席位密码";
            this.bunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton4.Iconimage = null;
            this.bunifuFlatButton4.Iconimage_right = null;
            this.bunifuFlatButton4.Iconimage_right_Selected = null;
            this.bunifuFlatButton4.Iconimage_Selected = null;
            this.bunifuFlatButton4.IconMarginLeft = 0;
            this.bunifuFlatButton4.IconMarginRight = 0;
            this.bunifuFlatButton4.IconRightVisible = false;
            this.bunifuFlatButton4.IconRightZoom = 0D;
            this.bunifuFlatButton4.IconVisible = false;
            this.bunifuFlatButton4.IconZoom = 90D;
            this.bunifuFlatButton4.IsTab = false;
            this.bunifuFlatButton4.Location = new System.Drawing.Point(1263, 192);
            this.bunifuFlatButton4.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuFlatButton4.Name = "bunifuFlatButton4";
            this.bunifuFlatButton4.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton4.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton4.selected = false;
            this.bunifuFlatButton4.Size = new System.Drawing.Size(326, 66);
            this.bunifuFlatButton4.TabIndex = 7;
            this.bunifuFlatButton4.Tag = "she";
            this.bunifuFlatButton4.Text = "重置席位密码";
            this.bunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton4.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton4.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 1;
            this.bunifuFlatButton5.ButtonText = "删除席位";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = null;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = false;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = false;
            this.bunifuFlatButton5.IconZoom = 90D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(1263, 117);
            this.bunifuFlatButton5.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(326, 66);
            this.bunifuFlatButton5.TabIndex = 6;
            this.bunifuFlatButton5.Text = "删除席位";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton6
            // 
            this.bunifuFlatButton6.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton6.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton6.BorderRadius = 1;
            this.bunifuFlatButton6.ButtonText = "新增席位";
            this.bunifuFlatButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton6.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton6.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton6.Iconimage = null;
            this.bunifuFlatButton6.Iconimage_right = null;
            this.bunifuFlatButton6.Iconimage_right_Selected = null;
            this.bunifuFlatButton6.Iconimage_Selected = null;
            this.bunifuFlatButton6.IconMarginLeft = 0;
            this.bunifuFlatButton6.IconMarginRight = 0;
            this.bunifuFlatButton6.IconRightVisible = false;
            this.bunifuFlatButton6.IconRightZoom = 0D;
            this.bunifuFlatButton6.IconVisible = false;
            this.bunifuFlatButton6.IconZoom = 90D;
            this.bunifuFlatButton6.IsTab = false;
            this.bunifuFlatButton6.Location = new System.Drawing.Point(1263, 42);
            this.bunifuFlatButton6.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuFlatButton6.Name = "bunifuFlatButton6";
            this.bunifuFlatButton6.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton6.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton6.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton6.selected = false;
            this.bunifuFlatButton6.Size = new System.Drawing.Size(326, 66);
            this.bunifuFlatButton6.TabIndex = 5;
            this.bunifuFlatButton6.Text = "新增席位";
            this.bunifuFlatButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton6.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton6.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuCustomLabel6
            // 
            this.bunifuCustomLabel6.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel6, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel6.Font = new System.Drawing.Font("Microsoft JhengHei", 11F);
            this.bunifuCustomLabel6.Location = new System.Drawing.Point(4, 6);
            this.bunifuCustomLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel6.Name = "bunifuCustomLabel6";
            this.bunifuCustomLabel6.Size = new System.Drawing.Size(100, 28);
            this.bunifuCustomLabel6.TabIndex = 4;
            this.bunifuCustomLabel6.Text = "席位信息";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Window;
            this.panel6.Controls.Add(this.dataGridView1);
            this.panel6.Controls.Add(this.bunifuFlatButton3);
            this.panel6.Controls.Add(this.bunifuFlatButton2);
            this.panel6.Controls.Add(this.bunifuFlatButton1);
            this.panel6.Controls.Add(this.bunifuCustomLabel9);
            this.panelAnimator.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(4, 208);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(2111, 286);
            this.panel6.TabIndex = 5;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dataGridView1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dataGridView1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dataGridView1, BunifuAnimatorNS.DecorationType.None);
            this.dataGridView1.Location = new System.Drawing.Point(50, 42);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1178, 216);
            this.dataGridView1.TabIndex = 10;
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 1;
            this.bunifuFlatButton3.ButtonText = "删除场下状态";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = null;
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = false;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = false;
            this.bunifuFlatButton3.IconZoom = 90D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(1263, 192);
            this.bunifuFlatButton3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(325, 66);
            this.bunifuFlatButton3.TabIndex = 7;
            this.bunifuFlatButton3.Text = "删除场下状态";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 1;
            this.bunifuFlatButton2.ButtonText = "修改场下状态";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = null;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = false;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = false;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(1263, 117);
            this.bunifuFlatButton2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(325, 66);
            this.bunifuFlatButton2.TabIndex = 6;
            this.bunifuFlatButton2.Text = "修改场下状态";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 1;
            this.bunifuFlatButton1.ButtonText = "新增场下状态";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = null;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = false;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = false;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(1263, 42);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.SteelBlue;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(325, 66);
            this.bunifuFlatButton1.TabIndex = 5;
            this.bunifuFlatButton1.Text = "新增场下状态";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuCustomLabel9
            // 
            this.bunifuCustomLabel9.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel9, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel9, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel9.Font = new System.Drawing.Font("Microsoft JhengHei", 11F);
            this.bunifuCustomLabel9.Location = new System.Drawing.Point(4, 6);
            this.bunifuCustomLabel9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel9.Name = "bunifuCustomLabel9";
            this.bunifuCustomLabel9.Size = new System.Drawing.Size(100, 28);
            this.bunifuCustomLabel9.TabIndex = 4;
            this.bunifuCustomLabel9.Text = "系统状态";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Window;
            this.panel5.Controls.Add(this.bunifuMetroTextbox2);
            this.panel5.Controls.Add(this.bunifuMetroTextbox1);
            this.panel5.Controls.Add(this.bunifuCustomLabel5);
            this.panel5.Controls.Add(this.bunifuCustomLabel4);
            this.panel5.Controls.Add(this.bunifuDropdown2);
            this.panel5.Controls.Add(this.bunifuCustomLabel3);
            this.panel5.Controls.Add(this.bunifuCustomLabel2);
            this.panelAnimator.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 98);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(2111, 110);
            this.panel5.TabIndex = 4;
            // 
            // bunifuMetroTextbox2
            // 
            this.bunifuMetroTextbox2.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuMetroTextbox2.BorderColorFocused = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox2.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox2.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox2.BorderThickness = 1;
            this.bunifuMetroTextbox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuTransition1.SetDecoration(this.bunifuMetroTextbox2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuMetroTextbox2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuMetroTextbox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuMetroTextbox2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMetroTextbox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox2.isPassword = false;
            this.bunifuMetroTextbox2.Location = new System.Drawing.Point(1263, 32);
            this.bunifuMetroTextbox2.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuMetroTextbox2.Name = "bunifuMetroTextbox2";
            this.bunifuMetroTextbox2.Size = new System.Drawing.Size(326, 58);
            this.bunifuMetroTextbox2.TabIndex = 11;
            this.bunifuMetroTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuMetroTextbox1
            // 
            this.bunifuMetroTextbox1.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuMetroTextbox1.BorderColorFocused = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox1.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox1.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox1.BorderThickness = 1;
            this.bunifuMetroTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuTransition1.SetDecoration(this.bunifuMetroTextbox1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuMetroTextbox1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuMetroTextbox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuMetroTextbox1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMetroTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox1.isPassword = false;
            this.bunifuMetroTextbox1.Location = new System.Drawing.Point(765, 32);
            this.bunifuMetroTextbox1.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuMetroTextbox1.Name = "bunifuMetroTextbox1";
            this.bunifuMetroTextbox1.Size = new System.Drawing.Size(326, 58);
            this.bunifuMetroTextbox1.TabIndex = 10;
            this.bunifuMetroTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel5, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel5, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(1144, 42);
            this.bunifuCustomLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(109, 30);
            this.bunifuCustomLabel5.TabIndex = 9;
            this.bunifuCustomLabel5.Text = "会员名称";
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel4, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel4, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(621, 42);
            this.bunifuCustomLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(133, 30);
            this.bunifuCustomLabel4.TabIndex = 7;
            this.bunifuCustomLabel4.Text = "交易所代码";
            // 
            // bunifuDropdown2
            // 
            this.bunifuDropdown2.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuDropdown2.BorderRadius = 1;
            this.bunifuTransition1.SetDecoration(this.bunifuDropdown2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuDropdown2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuDropdown2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuDropdown2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown2.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown2.Items = new string[0];
            this.bunifuDropdown2.Location = new System.Drawing.Point(200, 32);
            this.bunifuDropdown2.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuDropdown2.Name = "bunifuDropdown2";
            this.bunifuDropdown2.NomalColor = System.Drawing.Color.SteelBlue;
            this.bunifuDropdown2.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuDropdown2.selectedIndex = -1;
            this.bunifuDropdown2.Size = new System.Drawing.Size(326, 58);
            this.bunifuDropdown2.TabIndex = 4;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(69, 42);
            this.bunifuCustomLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(109, 30);
            this.bunifuCustomLabel3.TabIndex = 5;
            this.bunifuCustomLabel3.Text = "会员编号";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Microsoft JhengHei", 11F);
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(4, 6);
            this.bunifuCustomLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(100, 28);
            this.bunifuCustomLabel2.TabIndex = 4;
            this.bunifuCustomLabel2.Text = "会员信息";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Window;
            this.panel4.Controls.Add(this.bunifuCustomLabel1);
            this.panel4.Controls.Add(this.bunifuDropdown1);
            this.panelAnimator.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(2111, 94);
            this.panel4.TabIndex = 3;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(18, 28);
            this.bunifuCustomLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(157, 30);
            this.bunifuCustomLabel1.TabIndex = 3;
            this.bunifuCustomLabel1.Text = "经纪公司编号";
            // 
            // bunifuDropdown1
            // 
            this.bunifuDropdown1.BackColor = System.Drawing.Color.SteelBlue;
            this.bunifuDropdown1.BorderRadius = 3;
            this.bunifuTransition1.SetDecoration(this.bunifuDropdown1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuDropdown1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuDropdown1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuDropdown1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown1.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown1.Items = new string[0];
            this.bunifuDropdown1.Location = new System.Drawing.Point(200, 14);
            this.bunifuDropdown1.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuDropdown1.Name = "bunifuDropdown1";
            this.bunifuDropdown1.NomalColor = System.Drawing.Color.SteelBlue;
            this.bunifuDropdown1.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuDropdown1.selectedIndex = -1;
            this.bunifuDropdown1.Size = new System.Drawing.Size(326, 58);
            this.bunifuDropdown1.TabIndex = 2;
            // 
            // bunifuSwitch1
            // 
            this.bunifuSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch1.BorderRadius = 0;
            this.bunifuSwitch1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSwitch1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch1.Location = new System.Drawing.Point(232, 272);
            this.bunifuSwitch1.Margin = new System.Windows.Forms.Padding(11, 12, 11, 12);
            this.bunifuSwitch1.Name = "bunifuSwitch1";
            this.bunifuSwitch1.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch1.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch1.Size = new System.Drawing.Size(0, 0);
            this.bunifuSwitch1.TabIndex = 2;
            this.bunifuSwitch1.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch1.Value = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.Controls.Add(this.materialTabSelector2);
            this.panelAnimator.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2127, 42);
            this.panel2.TabIndex = 10;
            // 
            // materialTabSelector2
            // 
            this.materialTabSelector2.BaseTabControl = this.tabControl1;
            this.slide_panel.SetDecoration(this.materialTabSelector2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.materialTabSelector2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.materialTabSelector2, BunifuAnimatorNS.DecorationType.None);
            this.materialTabSelector2.Depth = 0;
            this.materialTabSelector2.Dock = System.Windows.Forms.DockStyle.Top;
            this.materialTabSelector2.Location = new System.Drawing.Point(0, 0);
            this.materialTabSelector2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector2.Name = "materialTabSelector2";
            this.materialTabSelector2.Size = new System.Drawing.Size(2127, 42);
            this.materialTabSelector2.TabIndex = 0;
            this.materialTabSelector2.Text = "materialTabSelector2";
            // 
            // trading
            // 
            this.trading.Controls.Add(this.materialTabControl2);
            this.trading.Controls.Add(this.materialTabSelector1);
            this.slide_panel.SetDecoration(this.trading, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.trading, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.trading, BunifuAnimatorNS.DecorationType.None);
            this.trading.Location = new System.Drawing.Point(54, 4);
            this.trading.Name = "trading";
            this.trading.Padding = new System.Windows.Forms.Padding(3);
            this.trading.Size = new System.Drawing.Size(2133, 972);
            this.trading.TabIndex = 1;
            this.trading.Text = "交易";
            this.trading.UseVisualStyleBackColor = true;
            // 
            // materialTabControl2
            // 
            this.materialTabControl2.Controls.Add(this.tab_orders);
            this.materialTabControl2.Controls.Add(this.tab_fills);
            this.materialTabControl2.Controls.Add(this.tab_positions);
            this.materialTabControl2.Controls.Add(this.tab_capital);
            this.materialTabControl2.Controls.Add(this.tab_fees);
            this.materialTabControl2.Controls.Add(this.tab_margin);
            this.materialTabControl2.Controls.Add(this.tab_moneytransfer);
            this.panelAnimator.SetDecoration(this.materialTabControl2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.materialTabControl2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.materialTabControl2, BunifuAnimatorNS.DecorationType.None);
            this.materialTabControl2.Depth = 0;
            this.materialTabControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.materialTabControl2.Location = new System.Drawing.Point(3, 45);
            this.materialTabControl2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl2.Name = "materialTabControl2";
            this.materialTabControl2.SelectedIndex = 0;
            this.materialTabControl2.Size = new System.Drawing.Size(2127, 928);
            this.materialTabControl2.TabIndex = 2;
            // 
            // tab_orders
            // 
            this.tab_orders.Controls.Add(this.dg_orders);
            this.tab_orders.Controls.Add(this.panel12);
            this.slide_panel.SetDecoration(this.tab_orders, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_orders, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_orders, BunifuAnimatorNS.DecorationType.None);
            this.tab_orders.Location = new System.Drawing.Point(4, 36);
            this.tab_orders.Name = "tab_orders";
            this.tab_orders.Padding = new System.Windows.Forms.Padding(3);
            this.tab_orders.Size = new System.Drawing.Size(2119, 888);
            this.tab_orders.TabIndex = 0;
            this.tab_orders.Text = "委托单查询";
            this.tab_orders.UseVisualStyleBackColor = true;
            // 
            // dg_orders
            // 
            this.dg_orders.AllowUserToAddRows = false;
            this.dg_orders.AllowUserToDeleteRows = false;
            this.dg_orders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_orders.ContextMenuStrip = this.orderCancel;
            this.slide_panel.SetDecoration(this.dg_orders, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_orders, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_orders, BunifuAnimatorNS.DecorationType.None);
            this.dg_orders.Dock = System.Windows.Forms.DockStyle.Top;
            this.dg_orders.Location = new System.Drawing.Point(3, 66);
            this.dg_orders.Name = "dg_orders";
            this.dg_orders.ReadOnly = true;
            this.dg_orders.RowTemplate.Height = 31;
            this.dg_orders.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dg_orders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_orders.Size = new System.Drawing.Size(2113, 826);
            this.dg_orders.TabIndex = 4;
            this.dg_orders.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_Orders_CellMouseDown);
            // 
            // orderCancel
            // 
            this.bunifuTransition1.SetDecoration(this.orderCancel, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.orderCancel, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.orderCancel, BunifuAnimatorNS.DecorationType.None);
            this.orderCancel.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.orderCancel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.orderCancel.Name = "orderCancel";
            this.orderCancel.Size = new System.Drawing.Size(117, 32);
            this.orderCancel.Click += new System.EventHandler(this.orderCancel_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(116, 28);
            this.toolStripMenuItem1.Text = "撤单";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.txtFilterOrderSym);
            this.panel12.Controls.Add(this.label42);
            this.panel12.Controls.Add(this.txtFilterOrderAccount);
            this.panel12.Controls.Add(this.label43);
            this.panel12.Controls.Add(this.label44);
            this.panelAnimator.SetDecoration(this.panel12, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel12, BunifuAnimatorNS.DecorationType.None);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(2113, 63);
            this.panel12.TabIndex = 3;
            // 
            // txtFilterOrderSym
            // 
            this.txtFilterOrderSym.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterOrderSym, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterOrderSym, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterOrderSym, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterOrderSym.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterOrderSym.Location = new System.Drawing.Point(1507, 14);
            this.txtFilterOrderSym.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterOrderSym.Name = "txtFilterOrderSym";
            this.txtFilterOrderSym.Size = new System.Drawing.Size(228, 36);
            this.txtFilterOrderSym.TabIndex = 116;
            this.txtFilterOrderSym.TextChanged += new System.EventHandler(this.txtFilterOrderSym_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label42, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label42, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label42, BunifuAnimatorNS.DecorationType.None);
            this.label42.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(1391, 19);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(110, 31);
            this.label42.TabIndex = 117;
            this.label42.Text = "合约代码";
            // 
            // txtFilterOrderAccount
            // 
            this.txtFilterOrderAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterOrderAccount, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterOrderAccount, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterOrderAccount, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterOrderAccount.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterOrderAccount.Location = new System.Drawing.Point(1073, 14);
            this.txtFilterOrderAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterOrderAccount.Name = "txtFilterOrderAccount";
            this.txtFilterOrderAccount.Size = new System.Drawing.Size(228, 36);
            this.txtFilterOrderAccount.TabIndex = 114;
            this.txtFilterOrderAccount.TextChanged += new System.EventHandler(this.txtFilterOrderAccount_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label43, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label43, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label43, BunifuAnimatorNS.DecorationType.None);
            this.label43.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(957, 19);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(110, 31);
            this.label43.TabIndex = 115;
            this.label43.Text = "客户代码";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label44, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label44, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label44, BunifuAnimatorNS.DecorationType.None);
            this.label44.Location = new System.Drawing.Point(848, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(74, 27);
            this.label44.TabIndex = 0;
            this.label44.Text = "FILTER";
            // 
            // tab_fills
            // 
            this.tab_fills.Controls.Add(this.dg_fills);
            this.tab_fills.Controls.Add(this.panel17);
            this.slide_panel.SetDecoration(this.tab_fills, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_fills, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_fills, BunifuAnimatorNS.DecorationType.None);
            this.tab_fills.Location = new System.Drawing.Point(4, 36);
            this.tab_fills.Name = "tab_fills";
            this.tab_fills.Padding = new System.Windows.Forms.Padding(3);
            this.tab_fills.Size = new System.Drawing.Size(2119, 888);
            this.tab_fills.TabIndex = 1;
            this.tab_fills.Text = "成交查询";
            this.tab_fills.UseVisualStyleBackColor = true;
            // 
            // dg_fills
            // 
            this.dg_fills.AllowUserToAddRows = false;
            this.dg_fills.AllowUserToDeleteRows = false;
            this.dg_fills.AllowUserToOrderColumns = true;
            this.dg_fills.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_fills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_fills, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_fills, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_fills, BunifuAnimatorNS.DecorationType.None);
            this.dg_fills.Dock = System.Windows.Forms.DockStyle.Top;
            this.dg_fills.Location = new System.Drawing.Point(3, 66);
            this.dg_fills.Name = "dg_fills";
            this.dg_fills.ReadOnly = true;
            this.dg_fills.RowTemplate.Height = 31;
            this.dg_fills.Size = new System.Drawing.Size(2113, 826);
            this.dg_fills.TabIndex = 5;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.txtFilterFillSym);
            this.panel17.Controls.Add(this.label50);
            this.panel17.Controls.Add(this.txtFilterFillAccount);
            this.panel17.Controls.Add(this.label51);
            this.panel17.Controls.Add(this.label52);
            this.panelAnimator.SetDecoration(this.panel17, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel17, BunifuAnimatorNS.DecorationType.None);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(3, 3);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(2113, 63);
            this.panel17.TabIndex = 4;
            // 
            // txtFilterFillSym
            // 
            this.txtFilterFillSym.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterFillSym, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterFillSym, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterFillSym, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterFillSym.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterFillSym.Location = new System.Drawing.Point(1507, 14);
            this.txtFilterFillSym.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterFillSym.Name = "txtFilterFillSym";
            this.txtFilterFillSym.Size = new System.Drawing.Size(228, 36);
            this.txtFilterFillSym.TabIndex = 116;
            this.txtFilterFillSym.TextChanged += new System.EventHandler(this.txtFilterFillSym_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label50, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label50, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label50, BunifuAnimatorNS.DecorationType.None);
            this.label50.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(1391, 19);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(110, 31);
            this.label50.TabIndex = 117;
            this.label50.Text = "合约代码";
            // 
            // txtFilterFillAccount
            // 
            this.txtFilterFillAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterFillAccount, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterFillAccount, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterFillAccount, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterFillAccount.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterFillAccount.Location = new System.Drawing.Point(1073, 14);
            this.txtFilterFillAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterFillAccount.Name = "txtFilterFillAccount";
            this.txtFilterFillAccount.Size = new System.Drawing.Size(228, 36);
            this.txtFilterFillAccount.TabIndex = 114;
            this.txtFilterFillAccount.TextChanged += new System.EventHandler(this.txtFilterFillAccount_TextChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label51, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label51, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label51, BunifuAnimatorNS.DecorationType.None);
            this.label51.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(957, 19);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(110, 31);
            this.label51.TabIndex = 115;
            this.label51.Text = "客户代码";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label52, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label52, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label52, BunifuAnimatorNS.DecorationType.None);
            this.label52.Location = new System.Drawing.Point(848, 23);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(74, 27);
            this.label52.TabIndex = 0;
            this.label52.Text = "FILTER";
            // 
            // tab_positions
            // 
            this.tab_positions.Controls.Add(this.panel18);
            this.slide_panel.SetDecoration(this.tab_positions, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_positions, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_positions, BunifuAnimatorNS.DecorationType.None);
            this.tab_positions.Location = new System.Drawing.Point(4, 36);
            this.tab_positions.Name = "tab_positions";
            this.tab_positions.Size = new System.Drawing.Size(2119, 888);
            this.tab_positions.TabIndex = 2;
            this.tab_positions.Text = "持仓查询";
            this.tab_positions.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.dg_positions);
            this.panel18.Controls.Add(this.panel9);
            this.panelAnimator.SetDecoration(this.panel18, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel18, BunifuAnimatorNS.DecorationType.None);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(2119, 888);
            this.panel18.TabIndex = 8;
            // 
            // dg_positions
            // 
            this.dg_positions.AllowUserToAddRows = false;
            this.dg_positions.AllowUserToDeleteRows = false;
            this.dg_positions.AllowUserToOrderColumns = true;
            this.dg_positions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_positions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_positions, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_positions, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_positions, BunifuAnimatorNS.DecorationType.None);
            this.dg_positions.Location = new System.Drawing.Point(3, 66);
            this.dg_positions.Name = "dg_positions";
            this.dg_positions.ReadOnly = true;
            this.dg_positions.RowTemplate.Height = 31;
            this.dg_positions.Size = new System.Drawing.Size(2117, 826);
            this.dg_positions.TabIndex = 8;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txt_filterSymPos);
            this.panel9.Controls.Add(this.label7);
            this.panel9.Controls.Add(this.txt_filterAccountPos);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.label9);
            this.panelAnimator.SetDecoration(this.panel9, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel9, BunifuAnimatorNS.DecorationType.None);
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(2117, 63);
            this.panel9.TabIndex = 7;
            // 
            // txt_filterSymPos
            // 
            this.txt_filterSymPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filterSymPos, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filterSymPos, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filterSymPos, BunifuAnimatorNS.DecorationType.None);
            this.txt_filterSymPos.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filterSymPos.Location = new System.Drawing.Point(1507, 14);
            this.txt_filterSymPos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filterSymPos.Name = "txt_filterSymPos";
            this.txt_filterSymPos.Size = new System.Drawing.Size(228, 36);
            this.txt_filterSymPos.TabIndex = 116;
            this.txt_filterSymPos.TextChanged += new System.EventHandler(this.txt_filterSymPos_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1391, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 31);
            this.label7.TabIndex = 117;
            this.label7.Text = "合约代码";
            // 
            // txt_filterAccountPos
            // 
            this.txt_filterAccountPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txt_filterAccountPos, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txt_filterAccountPos, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txt_filterAccountPos, BunifuAnimatorNS.DecorationType.None);
            this.txt_filterAccountPos.Font = new System.Drawing.Font("Roboto", 12F);
            this.txt_filterAccountPos.Location = new System.Drawing.Point(1073, 14);
            this.txt_filterAccountPos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_filterAccountPos.Name = "txt_filterAccountPos";
            this.txt_filterAccountPos.Size = new System.Drawing.Size(228, 36);
            this.txt_filterAccountPos.TabIndex = 114;
            this.txt_filterAccountPos.TextChanged += new System.EventHandler(this.txt_filterAccountPos_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.label8.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(957, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 31);
            this.label8.TabIndex = 115;
            this.label8.Text = "客户代码";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.label9.Location = new System.Drawing.Point(848, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 27);
            this.label9.TabIndex = 0;
            this.label9.Text = "FILTER";
            // 
            // tab_capital
            // 
            this.tab_capital.Controls.Add(this.dg_margin);
            this.tab_capital.Controls.Add(this.panel19);
            this.slide_panel.SetDecoration(this.tab_capital, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_capital, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_capital, BunifuAnimatorNS.DecorationType.None);
            this.tab_capital.Location = new System.Drawing.Point(4, 36);
            this.tab_capital.Name = "tab_capital";
            this.tab_capital.Padding = new System.Windows.Forms.Padding(3);
            this.tab_capital.Size = new System.Drawing.Size(2119, 888);
            this.tab_capital.TabIndex = 6;
            this.tab_capital.Text = "客户资金查询";
            this.tab_capital.UseVisualStyleBackColor = true;
            // 
            // dg_margin
            // 
            this.dg_margin.AllowUserToAddRows = false;
            this.dg_margin.AllowUserToDeleteRows = false;
            this.dg_margin.AllowUserToOrderColumns = true;
            this.dg_margin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_margin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_margin, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_margin, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_margin, BunifuAnimatorNS.DecorationType.None);
            this.dg_margin.Dock = System.Windows.Forms.DockStyle.Top;
            this.dg_margin.Location = new System.Drawing.Point(3, 66);
            this.dg_margin.Name = "dg_margin";
            this.dg_margin.ReadOnly = true;
            this.dg_margin.RowTemplate.Height = 31;
            this.dg_margin.Size = new System.Drawing.Size(2113, 826);
            this.dg_margin.TabIndex = 4;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.textBox6);
            this.panel19.Controls.Add(this.label53);
            this.panel19.Controls.Add(this.txtFilterCapitalAccount);
            this.panel19.Controls.Add(this.label54);
            this.panel19.Controls.Add(this.label55);
            this.panelAnimator.SetDecoration(this.panel19, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel19, BunifuAnimatorNS.DecorationType.None);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(3, 3);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(2113, 63);
            this.panel19.TabIndex = 3;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox6, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox6, BunifuAnimatorNS.DecorationType.None);
            this.textBox6.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox6.Location = new System.Drawing.Point(225, 14);
            this.textBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(228, 36);
            this.textBox6.TabIndex = 116;
            this.textBox6.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label53, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label53, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label53, BunifuAnimatorNS.DecorationType.None);
            this.label53.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(494, 19);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(110, 31);
            this.label53.TabIndex = 117;
            this.label53.Text = "合约代码";
            this.label53.Visible = false;
            // 
            // txtFilterCapitalAccount
            // 
            this.txtFilterCapitalAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterCapitalAccount, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterCapitalAccount, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterCapitalAccount, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterCapitalAccount.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterCapitalAccount.Location = new System.Drawing.Point(1507, 14);
            this.txtFilterCapitalAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterCapitalAccount.Name = "txtFilterCapitalAccount";
            this.txtFilterCapitalAccount.Size = new System.Drawing.Size(228, 36);
            this.txtFilterCapitalAccount.TabIndex = 114;
            this.txtFilterCapitalAccount.TextChanged += new System.EventHandler(this.txtFilterCapitalAccount_TextChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label54, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label54, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label54, BunifuAnimatorNS.DecorationType.None);
            this.label54.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(1391, 19);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(110, 31);
            this.label54.TabIndex = 115;
            this.label54.Text = "客户代码";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label55, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label55, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label55, BunifuAnimatorNS.DecorationType.None);
            this.label55.Location = new System.Drawing.Point(1293, 23);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(74, 27);
            this.label55.TabIndex = 0;
            this.label55.Text = "FILTER";
            // 
            // tab_fees
            // 
            this.tab_fees.Controls.Add(this.dg_fee);
            this.tab_fees.Controls.Add(this.panel3);
            this.slide_panel.SetDecoration(this.tab_fees, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_fees, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_fees, BunifuAnimatorNS.DecorationType.None);
            this.tab_fees.Location = new System.Drawing.Point(4, 36);
            this.tab_fees.Name = "tab_fees";
            this.tab_fees.Size = new System.Drawing.Size(2119, 888);
            this.tab_fees.TabIndex = 4;
            this.tab_fees.Text = "手续费查询";
            this.tab_fees.UseVisualStyleBackColor = true;
            // 
            // dg_fee
            // 
            this.dg_fee.AllowUserToAddRows = false;
            this.dg_fee.AllowUserToDeleteRows = false;
            this.dg_fee.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_fee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_fee, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_fee, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_fee, BunifuAnimatorNS.DecorationType.None);
            this.dg_fee.Dock = System.Windows.Forms.DockStyle.Left;
            this.dg_fee.Location = new System.Drawing.Point(0, 63);
            this.dg_fee.Name = "dg_fee";
            this.dg_fee.RowTemplate.Height = 31;
            this.dg_fee.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dg_fee.Size = new System.Drawing.Size(2120, 825);
            this.dg_fee.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtFilterFeeSymbol);
            this.panel3.Controls.Add(this.label39);
            this.panel3.Controls.Add(this.txtFilterFeeAccount);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.label41);
            this.panelAnimator.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2119, 63);
            this.panel3.TabIndex = 1;
            // 
            // txtFilterFeeSymbol
            // 
            this.txtFilterFeeSymbol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterFeeSymbol, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterFeeSymbol, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterFeeSymbol, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterFeeSymbol.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterFeeSymbol.Location = new System.Drawing.Point(1507, 14);
            this.txtFilterFeeSymbol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterFeeSymbol.Name = "txtFilterFeeSymbol";
            this.txtFilterFeeSymbol.Size = new System.Drawing.Size(228, 36);
            this.txtFilterFeeSymbol.TabIndex = 116;
            this.txtFilterFeeSymbol.TextChanged += new System.EventHandler(this.txtFilterFeeSymbol_TextChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label39, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label39, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label39, BunifuAnimatorNS.DecorationType.None);
            this.label39.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(1391, 19);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(110, 31);
            this.label39.TabIndex = 117;
            this.label39.Text = "合约代码";
            // 
            // txtFilterFeeAccount
            // 
            this.txtFilterFeeAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterFeeAccount, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterFeeAccount, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterFeeAccount, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterFeeAccount.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterFeeAccount.Location = new System.Drawing.Point(1073, 14);
            this.txtFilterFeeAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterFeeAccount.Name = "txtFilterFeeAccount";
            this.txtFilterFeeAccount.Size = new System.Drawing.Size(228, 36);
            this.txtFilterFeeAccount.TabIndex = 114;
            this.txtFilterFeeAccount.TextChanged += new System.EventHandler(this.txtFilterFeeAccount_TextChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label40, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label40, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label40, BunifuAnimatorNS.DecorationType.None);
            this.label40.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(957, 19);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(110, 31);
            this.label40.TabIndex = 115;
            this.label40.Text = "客户代码";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label41, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label41, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label41, BunifuAnimatorNS.DecorationType.None);
            this.label41.Location = new System.Drawing.Point(16, 18);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(74, 27);
            this.label41.TabIndex = 0;
            this.label41.Text = "FILTER";
            // 
            // tab_margin
            // 
            this.tab_margin.Controls.Add(this.dg_marginrate);
            this.tab_margin.Controls.Add(this.panel13);
            this.slide_panel.SetDecoration(this.tab_margin, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_margin, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_margin, BunifuAnimatorNS.DecorationType.None);
            this.tab_margin.Location = new System.Drawing.Point(4, 36);
            this.tab_margin.Name = "tab_margin";
            this.tab_margin.Size = new System.Drawing.Size(2119, 888);
            this.tab_margin.TabIndex = 5;
            this.tab_margin.Text = "保证金查询";
            this.tab_margin.UseVisualStyleBackColor = true;
            // 
            // dg_marginrate
            // 
            this.dg_marginrate.AllowUserToAddRows = false;
            this.dg_marginrate.AllowUserToDeleteRows = false;
            this.dg_marginrate.AllowUserToOrderColumns = true;
            this.dg_marginrate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_marginrate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slide_panel.SetDecoration(this.dg_marginrate, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dg_marginrate, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dg_marginrate, BunifuAnimatorNS.DecorationType.None);
            this.dg_marginrate.Dock = System.Windows.Forms.DockStyle.Top;
            this.dg_marginrate.Location = new System.Drawing.Point(0, 63);
            this.dg_marginrate.Name = "dg_marginrate";
            this.dg_marginrate.ReadOnly = true;
            this.dg_marginrate.RowTemplate.Height = 31;
            this.dg_marginrate.Size = new System.Drawing.Size(2119, 826);
            this.dg_marginrate.TabIndex = 6;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtFilterMarginSymbol);
            this.panel13.Controls.Add(this.label1);
            this.panel13.Controls.Add(this.txtFilterMarginAccount);
            this.panel13.Controls.Add(this.label2);
            this.panel13.Controls.Add(this.label5);
            this.panelAnimator.SetDecoration(this.panel13, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.panel13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel13, BunifuAnimatorNS.DecorationType.None);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(2119, 63);
            this.panel13.TabIndex = 4;
            // 
            // txtFilterMarginSymbol
            // 
            this.txtFilterMarginSymbol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterMarginSymbol, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterMarginSymbol, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterMarginSymbol, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterMarginSymbol.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterMarginSymbol.Location = new System.Drawing.Point(1507, 14);
            this.txtFilterMarginSymbol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterMarginSymbol.Name = "txtFilterMarginSymbol";
            this.txtFilterMarginSymbol.Size = new System.Drawing.Size(228, 36);
            this.txtFilterMarginSymbol.TabIndex = 116;
            this.txtFilterMarginSymbol.TextChanged += new System.EventHandler(this.txtFilterMarginSymbol_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1391, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 31);
            this.label1.TabIndex = 117;
            this.label1.Text = "合约代码";
            // 
            // txtFilterMarginAccount
            // 
            this.txtFilterMarginAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.txtFilterMarginAccount, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.txtFilterMarginAccount, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.txtFilterMarginAccount, BunifuAnimatorNS.DecorationType.None);
            this.txtFilterMarginAccount.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtFilterMarginAccount.Location = new System.Drawing.Point(1073, 14);
            this.txtFilterMarginAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFilterMarginAccount.Name = "txtFilterMarginAccount";
            this.txtFilterMarginAccount.Size = new System.Drawing.Size(228, 36);
            this.txtFilterMarginAccount.TabIndex = 114;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(957, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 31);
            this.label2.TabIndex = 115;
            this.label2.Text = "客户代码";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.label5.Location = new System.Drawing.Point(16, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 27);
            this.label5.TabIndex = 0;
            this.label5.Text = "FILTER";
            // 
            // tab_moneytransfer
            // 
            this.slide_panel.SetDecoration(this.tab_moneytransfer, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tab_moneytransfer, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tab_moneytransfer, BunifuAnimatorNS.DecorationType.None);
            this.tab_moneytransfer.Location = new System.Drawing.Point(4, 36);
            this.tab_moneytransfer.Name = "tab_moneytransfer";
            this.tab_moneytransfer.Size = new System.Drawing.Size(2119, 888);
            this.tab_moneytransfer.TabIndex = 3;
            this.tab_moneytransfer.Text = "出金查询";
            this.tab_moneytransfer.UseVisualStyleBackColor = true;
            // 
            // materialTabSelector1
            // 
            this.materialTabSelector1.BaseTabControl = this.materialTabControl2;
            this.slide_panel.SetDecoration(this.materialTabSelector1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.materialTabSelector1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.materialTabSelector1, BunifuAnimatorNS.DecorationType.None);
            this.materialTabSelector1.Depth = 0;
            this.materialTabSelector1.Dock = System.Windows.Forms.DockStyle.Top;
            this.materialTabSelector1.Location = new System.Drawing.Point(3, 3);
            this.materialTabSelector1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector1.Name = "materialTabSelector1";
            this.materialTabSelector1.Size = new System.Drawing.Size(2127, 42);
            this.materialTabSelector1.TabIndex = 1;
            this.materialTabSelector1.Text = "materialTabSelector1";
            // 
            // 费管理
            // 
            this.费管理.BackColor = System.Drawing.Color.White;
            this.费管理.Controls.Add(this.bunifuFlatButton17);
            this.费管理.Controls.Add(this.textBox27);
            this.费管理.Controls.Add(this.label48);
            this.费管理.Controls.Add(this.textBox28);
            this.费管理.Controls.Add(this.label49);
            this.费管理.Controls.Add(this.textBox26);
            this.费管理.Controls.Add(this.label47);
            this.费管理.Controls.Add(this.textBox25);
            this.费管理.Controls.Add(this.label46);
            this.费管理.Controls.Add(this.textBox24);
            this.费管理.Controls.Add(this.label45);
            this.费管理.Controls.Add(this.dataGridView11);
            this.费管理.Controls.Add(this.label6);
            this.费管理.Controls.Add(this.comboBox3);
            this.费管理.Controls.Add(this.label3);
            this.费管理.Controls.Add(this.comboBox2);
            this.slide_panel.SetDecoration(this.费管理, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.费管理, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.费管理, BunifuAnimatorNS.DecorationType.None);
            this.费管理.Location = new System.Drawing.Point(54, 4);
            this.费管理.Name = "费管理";
            this.费管理.Padding = new System.Windows.Forms.Padding(3);
            this.费管理.Size = new System.Drawing.Size(2133, 972);
            this.费管理.TabIndex = 2;
            this.费管理.Text = "费管理";
            // 
            // bunifuFlatButton17
            // 
            this.bunifuFlatButton17.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton17.BorderRadius = 0;
            this.bunifuFlatButton17.ButtonText = "Save";
            this.bunifuFlatButton17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.bunifuFlatButton17, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.bunifuFlatButton17, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.bunifuFlatButton17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton17.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton17.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton17.Iconimage = null;
            this.bunifuFlatButton17.Iconimage_right = null;
            this.bunifuFlatButton17.Iconimage_right_Selected = null;
            this.bunifuFlatButton17.Iconimage_Selected = null;
            this.bunifuFlatButton17.IconMarginLeft = 0;
            this.bunifuFlatButton17.IconMarginRight = 0;
            this.bunifuFlatButton17.IconRightVisible = false;
            this.bunifuFlatButton17.IconRightZoom = 0D;
            this.bunifuFlatButton17.IconVisible = false;
            this.bunifuFlatButton17.IconZoom = 90D;
            this.bunifuFlatButton17.IsTab = false;
            this.bunifuFlatButton17.Location = new System.Drawing.Point(1279, 27);
            this.bunifuFlatButton17.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.bunifuFlatButton17.Name = "bunifuFlatButton17";
            this.bunifuFlatButton17.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton17.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuFlatButton17.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton17.selected = false;
            this.bunifuFlatButton17.Size = new System.Drawing.Size(122, 200);
            this.bunifuFlatButton17.TabIndex = 142;
            this.bunifuFlatButton17.Text = "Save";
            this.bunifuFlatButton17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton17.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton17.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // textBox27
            // 
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox27, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox27, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox27, BunifuAnimatorNS.DecorationType.None);
            this.textBox27.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox27.Location = new System.Drawing.Point(975, 191);
            this.textBox27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(259, 36);
            this.textBox27.TabIndex = 140;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label48, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label48, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label48, BunifuAnimatorNS.DecorationType.None);
            this.label48.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(727, 191);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(230, 31);
            this.label48.TabIndex = 141;
            this.label48.Text = "平仓交易手续费价格";
            // 
            // textBox28
            // 
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox28, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox28, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox28, BunifuAnimatorNS.DecorationType.None);
            this.textBox28.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox28.Location = new System.Drawing.Point(975, 136);
            this.textBox28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(259, 36);
            this.textBox28.TabIndex = 138;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label49, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label49, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label49, BunifuAnimatorNS.DecorationType.None);
            this.label49.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(727, 136);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(230, 31);
            this.label49.TabIndex = 139;
            this.label49.Text = "开仓交易手续费价格";
            // 
            // textBox26
            // 
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox26, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox26, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox26, BunifuAnimatorNS.DecorationType.None);
            this.textBox26.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox26.Location = new System.Drawing.Point(277, 191);
            this.textBox26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(259, 36);
            this.textBox26.TabIndex = 136;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label47, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label47, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label47, BunifuAnimatorNS.DecorationType.None);
            this.label47.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(65, 196);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(206, 31);
            this.label47.TabIndex = 137;
            this.label47.Text = "平仓交易手续费％";
            // 
            // textBox25
            // 
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox25, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox25, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox25, BunifuAnimatorNS.DecorationType.None);
            this.textBox25.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox25.Location = new System.Drawing.Point(277, 136);
            this.textBox25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(259, 36);
            this.textBox25.TabIndex = 134;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label46, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label46, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label46, BunifuAnimatorNS.DecorationType.None);
            this.label46.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(65, 141);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(206, 31);
            this.label46.TabIndex = 135;
            this.label46.Text = "开仓交易手续费％";
            // 
            // textBox24
            // 
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.textBox24, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.textBox24, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.textBox24, BunifuAnimatorNS.DecorationType.None);
            this.textBox24.Font = new System.Drawing.Font("Roboto", 12F);
            this.textBox24.Location = new System.Drawing.Point(277, 82);
            this.textBox24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(259, 36);
            this.textBox24.TabIndex = 130;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label45, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label45, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label45, BunifuAnimatorNS.DecorationType.None);
            this.label45.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(100, 87);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(158, 31);
            this.label45.TabIndex = 131;
            this.label45.Text = "投机套保标识";
            // 
            // dataGridView11
            // 
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn45,
            this.Column20,
            this.Column21,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47});
            this.slide_panel.SetDecoration(this.dataGridView11, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.dataGridView11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.dataGridView11, BunifuAnimatorNS.DecorationType.None);
            this.dataGridView11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView11.Location = new System.Drawing.Point(3, 281);
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.RowTemplate.Height = 31;
            this.dataGridView11.Size = new System.Drawing.Size(2127, 688);
            this.dataGridView11.TabIndex = 107;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.HeaderText = "品种代码";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.Width = 150;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "合约代码";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.Width = 150;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.HeaderText = "投机套保标识";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.Width = 150;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.HeaderText = "开仓交易手续费";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Width = 150;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "平仓交易手续费";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.Width = 150;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.HeaderText = "经纪公司编号";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.Width = 150;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "平昨交易手续费";
            this.Column20.Name = "Column20";
            this.Column20.Width = 150;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "平今交易手续费";
            this.Column21.Name = "Column21";
            this.Column21.Width = 150;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.HeaderText = "交易编码";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.Width = 150;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "交易所编码";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.Width = 150;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(847, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 31);
            this.label6.TabIndex = 106;
            this.label6.Text = "模板名称";
            // 
            // comboBox3
            // 
            this.slide_panel.SetDecoration(this.comboBox3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.comboBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.comboBox3, BunifuAnimatorNS.DecorationType.None);
            this.comboBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox3.Font = new System.Drawing.Font("Roboto", 12F);
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(973, 33);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(260, 37);
            this.comboBox3.TabIndex = 105;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(88, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 31);
            this.label3.TabIndex = 104;
            this.label3.Text = "手续费模板ＩＤ";
            // 
            // comboBox2
            // 
            this.slide_panel.SetDecoration(this.comboBox2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.comboBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.comboBox2, BunifuAnimatorNS.DecorationType.None);
            this.comboBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox2.Font = new System.Drawing.Font("Roboto", 12F);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(276, 27);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(260, 37);
            this.comboBox2.TabIndex = 103;
            // 
            // tabPage11
            // 
            this.slide_panel.SetDecoration(this.tabPage11, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.tabPage11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.tabPage11, BunifuAnimatorNS.DecorationType.None);
            this.tabPage11.Location = new System.Drawing.Point(54, 4);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(2133, 972);
            this.tabPage11.TabIndex = 3;
            this.tabPage11.Text = "出金管理";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip2
            // 
            this.bunifuTransition1.SetDecoration(this.contextMenuStrip2, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.contextMenuStrip2, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this.contextMenuStrip2, BunifuAnimatorNS.DecorationType.None);
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // doubleBitmapControl1
            // 
            this.slide_panel.SetDecoration(this.doubleBitmapControl1, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this.doubleBitmapControl1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.doubleBitmapControl1, BunifuAnimatorNS.DecorationType.None);
            this.doubleBitmapControl1.Location = new System.Drawing.Point(2005, 1014);
            this.doubleBitmapControl1.Name = "doubleBitmapControl1";
            this.doubleBitmapControl1.Size = new System.Drawing.Size(75, 23);
            this.doubleBitmapControl1.TabIndex = 14;
            this.doubleBitmapControl1.Text = "doubleBitmapControl1";
            this.doubleBitmapControl1.Visible = false;
            // 
            // timer_Tick
            // 
            this.timer_Tick.Interval = 1000;
            // 
            // slide_panel
            // 
            this.slide_panel.AnimationType = BunifuAnimatorNS.AnimationType.Scale;
            this.slide_panel.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.slide_panel.DefaultAnimation = animation2;
            this.slide_panel.MaxAnimationTime = 1000;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuDragControl2
            // 
            this.bunifuDragControl2.Fixed = false;
            this.bunifuDragControl2.Horizontal = true;
            this.bunifuDragControl2.TargetControl = this.pnl_addUser;
            this.bunifuDragControl2.Vertical = true;
            // 
            // bunifuDragControl3
            // 
            this.bunifuDragControl3.Fixed = false;
            this.bunifuDragControl3.Horizontal = true;
            this.bunifuDragControl3.TargetControl = this.pnl_add_investor;
            this.bunifuDragControl3.Vertical = true;
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.VertSlide;
            this.bunifuTransition1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.bunifuTransition1.DefaultAnimation = animation1;
            this.bunifuTransition1.TimeStep = 0.06F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(2321, 1037);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.doubleBitmapControl1);
            this.Controls.Add(this.header);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.panelAnimator.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.slide_panel.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.materialTabControl1.ResumeLayout(false);
            this.account1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.Trader.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.pnl_addUser.ResumeLayout(false);
            this.pnl_addUser.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_users)).EndInit();
            this.pnl_user.ResumeLayout(false);
            this.pnl_user.PerformLayout();
            this.pnl_user_Search2.ResumeLayout(false);
            this.pnl_user_Search2.PerformLayout();
            this.Investor.ResumeLayout(false);
            this.pnl_dg_investor.ResumeLayout(false);
            this.pnl_add_investor.ResumeLayout(false);
            this.pnl_add_investor.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_InvestorDetails)).EndInit();
            this.ctx_investor.ResumeLayout(false);
            this.pnl_investor.ResumeLayout(false);
            this.pnl_investor_add.ResumeLayout(false);
            this.pnl_investor_add.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_AccountMap)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_loginUsers)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.Account.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.trading.ResumeLayout(false);
            this.materialTabControl2.ResumeLayout(false);
            this.tab_orders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_orders)).EndInit();
            this.orderCancel.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.tab_fills.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_fills)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.tab_positions.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_positions)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tab_capital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_margin)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.tab_fees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_fee)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tab_margin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_marginrate)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.费管理.ResumeLayout(false);
            this.费管理.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private BunifuAnimatorNS.BunifuTransition panelAnimator;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
        private System.ComponentModel.BackgroundWorker backgroundWorker4;
        private System.ComponentModel.BackgroundWorker backgroundWorker5;
        private System.ComponentModel.BackgroundWorker backgroundWorker6;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer timer_Tick;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label materialLabel1;
        private BunifuAnimatorNS.BunifuTransition slide_panel;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl2;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl3;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl materialTabControl1;
        private System.Windows.Forms.TabPage account1;
        private MaterialSkin.Controls.MaterialTabControl tabControl1;
        private System.Windows.Forms.TabPage Account;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton4;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton6;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel9;
        private System.Windows.Forms.Panel panel5;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox2;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.Panel panel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown1;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch1;
        private System.Windows.Forms.TabPage Trader;
        private System.Windows.Forms.Panel pnl_user;
        private System.Windows.Forms.TabPage Investor;
        private System.Windows.Forms.Panel pnl_investor;
        private System.Windows.Forms.TabPage tabPage4;
        public System.Windows.Forms.DataGridView dg_AccountMap;
        private System.Windows.Forms.Panel panel15;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton11;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton12;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.DataGridView dg_loginUsers;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage TransferMoney;
        private System.Windows.Forms.Panel panel2;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector2;
        private System.Windows.Forms.TabPage trading;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl2;
        private System.Windows.Forms.TabPage tab_orders;
        private System.Windows.Forms.TabPage tab_fills;
        private System.Windows.Forms.TabPage tab_capital;
        private System.Windows.Forms.TabPage tab_fees;
        public System.Windows.Forms.DataGridView dg_fee;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtFilterFeeSymbol;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtFilterFeeAccount;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TabPage tab_margin;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtFilterMarginSymbol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFilterMarginAccount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tab_moneytransfer;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
        private System.Windows.Forms.TabPage 费管理;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton17;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TabPage tabPage11;
        private Bunifu.Framework.UI.BunifuFlatButton btn_header_accounts;
        private Bunifu.Framework.UI.BunifuFlatButton btn_header_capital;
        private Bunifu.Framework.UI.BunifuFlatButton btn_header_fees;
        private Bunifu.Framework.UI.BunifuFlatButton btn_header_trading;
        private System.Windows.Forms.Button btnExpInvestor;
        private System.Windows.Forms.Button btn_user_collapse;
        private System.Windows.Forms.Panel pnl_user_Search2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton15;
        private System.Windows.Forms.TextBox txt_filter_traderId;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_filter_brokerNum;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_filter_sysNum;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        public System.Windows.Forms.DataGridView dg_orders;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtFilterOrderSym;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtFilterOrderAccount;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        public System.Windows.Forms.DataGridView dg_fills;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox txtFilterFillSym;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtFilterFillAccount;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        public System.Windows.Forms.DataGridView dg_margin;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtFilterCapitalAccount;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.TextBox txt_filter_loginUser;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private Bunifu.Framework.UI.BunifuFlatButton btn_addAccountMap;
        public System.Windows.Forms.ComboBox cmb_account;
        public System.Windows.Forms.ComboBox cmb_username;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private MaterialSkin.Controls.MaterialToolStripMenuItem DeleteRow;
        private System.Windows.Forms.TabPage tab_positions;
        private System.Windows.Forms.Panel panel18;
        public System.Windows.Forms.DataGridView dg_positions;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txt_filterSymPos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_filterAccountPos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.DataGridView dg_marginrate;
        private System.Windows.Forms.ContextMenuStrip orderCancel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Panel pnl_investor_add;
        private Bunifu.Framework.UI.BunifuFlatButton btn_investor_open_add;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txt_investor_account;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ContextMenuStrip ctx_investor;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ComboBox txt_investor_exch;
        private BunifuAnimatorNS.DoubleBitmapControl doubleBitmapControl1;
        private System.Windows.Forms.Panel pnl_dg_investor;
        private System.Windows.Forms.Panel pnl_add_investor;
        private System.Windows.Forms.Button btn_close_addInvestor;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.ComboBox cmb_investor_exch_add;
        private System.Windows.Forms.TextBox txt_investor_mm_add;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txt_investor_hedge_add;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txt_investor_arbitrage_add;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txt_investor_speculate_add;
        private System.Windows.Forms.Label label38;
        private Bunifu.Framework.UI.BunifuFlatButton btn_investor_add;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txt_investor_account_add;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        public System.Windows.Forms.DataGridView dg_InvestorDetails;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel pnl_addUser;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckBox chk_riskman;
        private System.Windows.Forms.CheckBox chk_quote;
        private System.Windows.Forms.CheckBox chk_trade;
        private System.Windows.Forms.CheckBox chk_admin;
        private System.Windows.Forms.Label lbl_passwordmatch;
        private Bunifu.Framework.UI.BunifuFlatButton btn_panelAddUserSave;
        private System.Windows.Forms.TextBox txt_newTraderId;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_newTraderType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_newTraderPass2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_newTraderPass;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txt_newTraderBroker;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_newTraderDept;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_newTraderName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_newTraderSystemNum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.DataGridView dg_users;
        private System.Windows.Forms.Label lbl_save_investor_error;
    }
}


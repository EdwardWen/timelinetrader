// TLCClientApiTrade_CLI.h

#pragma once
#include <cstring>
#include <vcclr.h>
#include <TLCClientApiTrade.h>

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace TLCClientApi_CLI;

using OrderRequestAck = Reply;
using OrderCancelRequestAck = Reply;
using OrderCancelAck = Reply;
using OrderCancelReject = Reject;
using QryOrderRequest = AccountRequest;
using QryFillRequest = AccountRequest;
using QryPositionRequest = AccountRequest;

namespace TLCClientApiTrade_CLI {

	class TradeSpiImp;

	public ref class TradeSpiWrapper abstract
	{
	public:
		TradeSpiImp * imp;
		TradeSpiWrapper();

		virtual void onConnected() abstract;
		virtual void onDisconnected() abstract;
		virtual void onRspLogin(LoginReply^ reply) abstract;
		virtual void onRspLogout(LogoutReply^ reply) abstract;
		virtual void onRspQryAccount(String^ account, bool isLast) abstract;
		virtual void onRspNewOrder(OrderRequestAck^ reply) abstract;
		virtual void onRtnNewOrder(Order^ reply) abstract;
		virtual void onErrorRtnNewOrder(OrderReject^ reply) abstract;
		virtual void onRspCancelOrder(OrderCancelRequestAck^ reply) abstract;
		virtual void onRtnCancelOrder(OrderCancelAck^ reply) abstract;
		virtual void onErrorRtnCancelOrder(OrderCancelReject^ reply) abstract;
		virtual void onRtnOrderFill(Fill^ fill) abstract;
		virtual void onRspQryOrder(Order^ order, bool isLast) abstract;
		virtual void onRspQryFill(Fill^ fill, bool isLast) abstract;
	};

	class TradeSpiImp : public TLCClientApi::TradeSpi
	{
	public:
		gcroot<TradeSpiWrapper^> wrapper;
		TradeSpiImp(TradeSpiWrapper^ wrapper) : wrapper(wrapper) {}
		virtual void onConnected()
		{
			wrapper->onConnected();
		}

		virtual void onRspLogin(TLCClientApi::LoginReply &reply)
		{
			LoginReply^ r = gcnew LoginReply();
			r->ret = (ReturnType)(reply.ret);
			r->username = gcnew String(reply.username);
			wrapper->onRspLogin(r);
		};

		virtual void onRspLogout(TLCClientApi::LogoutReply &reply)
		{
			LogoutReply^ r = gcnew LogoutReply();
			r->ret = (ReturnType)(reply.ret);
			wrapper->onRspLogout(r);
		}

		virtual void onRspQryAccount(const char *account, bool isLast)
		{
			String^ accountStr = gcnew String(account);
			wrapper->onRspQryAccount(accountStr, isLast);
		}

		virtual void onDisconnected()
		{
			wrapper->onDisconnected();
		}

		virtual void onRspNewOrder(const TLCClientApi::OrderRequestAck &reply)
		{
			OrderRequestAck^ r = gcnew OrderRequestAck();
			r->id = gcnew OrderId();
			r->id->clOrdId = reply.id.clOrdId;
			r->id->ordId = reply.id.ordId;
			wrapper->onRspNewOrder(r);
		}

		virtual void onRtnNewOrder(const TLCClientApi::Order &reply)
		{
			Order^ r = gcnew Order();
			r->reply = gcnew Reply();
			r->reply->id = gcnew OrderId();
			r->reply->id->clOrdId = reply.reply.id.clOrdId;
			r->reply->id->ordId = reply.reply.id.ordId;
			r->account = gcnew String(reply.account);
			r->symbol = gcnew String(reply.symbol);
			r->timeInForce = (TimeInForce)(reply.timeInForce);
			r->side = (Side)(reply.side);
			r->qty = reply.qty;
			r->price = reply.price;
			r->positionOffset = (PositionOffset)reply.positionOffset;
			wrapper->onRtnNewOrder(r);
		}

		virtual void onErrorRtnNewOrder(const TLCClientApi::OrderReject &reply)
		{
			OrderReject^ r = gcnew OrderReject();
			r->reject = gcnew Reject();
			r->reject->id = gcnew OrderId();
			r->reject->id->clOrdId = reply.reject.id.clOrdId;
			r->reject->id->ordId = reply.reject.id.ordId;
			r->reject->reason = (ReturnType)reply.reject.reason;
			r->account = gcnew String(reply.account);
			r->symbol = gcnew String(reply.symbol);
			r->timeInForce = (TimeInForce)(reply.timeInForce);
			r->side = (Side)(reply.side);
			r->qty = reply.qty;
			r->price = reply.price;
			r->positionOffset = (PositionOffset)reply.positionOffset;
			wrapper->onErrorRtnNewOrder(r);
		}

		virtual void onRspCancelOrder(const TLCClientApi::OrderCancelRequestAck &reply)
		{
			OrderCancelRequestAck^ r = gcnew OrderCancelRequestAck();
			r->id = gcnew OrderId();
			r->id->clOrdId = reply.id.clOrdId;
			r->id->ordId = reply.id.ordId;
			wrapper->onRspCancelOrder(r);
		}

		virtual void onRtnCancelOrder(const TLCClientApi::OrderCancelAck &reply)
		{
			OrderCancelAck^ r = gcnew OrderCancelAck();
			r->id = gcnew OrderId();
			r->id->clOrdId = reply.id.clOrdId;
			r->id->ordId = reply.id.ordId;
			wrapper->onRtnCancelOrder(r);
		}

		virtual void onErrorRtnCancelOrder(const TLCClientApi::OrderCancelReject &reply)
		{
			OrderCancelReject^ r = gcnew OrderCancelReject();
			r->id = gcnew OrderId();
			r->id->clOrdId = reply.id.clOrdId;
			r->id->ordId = reply.id.ordId;
			wrapper->onErrorRtnCancelOrder(r);
		}

		virtual void onRtnOrderFill(const TLCClientApi::Fill &fill)
		{
			Fill^ r = gcnew Fill();
			r->id = gcnew OrderId();
			r->id->clOrdId = fill.id.clOrdId;
			r->id->ordId = fill.id.ordId;
			r->qty = fill.qty;
			r->price = fill.price;
			r->commission = fill.commission;
			wrapper->onRtnOrderFill(r);
		}

		virtual void onRspQryOrder(const TLCClientApi::Order &reply, bool isLast)
		{
			Order^ r = gcnew Order();
			r->reply = gcnew Reply();
			r->reply->id = gcnew OrderId();
			r->reply->id->clOrdId = reply.reply.id.clOrdId;
			r->reply->id->ordId = reply.reply.id.ordId;
			r->account = gcnew String(reply.account);
			r->symbol = gcnew String(reply.symbol);
			r->timeInForce = (TimeInForce)(reply.timeInForce);
			r->side = (Side)(reply.side);
			r->qty = reply.qty;
			r->price = reply.price;
			r->positionOffset = (PositionOffset)reply.positionOffset;
			wrapper->onRspQryOrder(r, isLast);
		}

		virtual void onRspQryFill(const TLCClientApi::Fill &fill, bool isLast)
		{
			Fill^ r = gcnew Fill();
			r->id = gcnew OrderId();
			r->id->clOrdId = fill.id.clOrdId;
			r->id->ordId = fill.id.ordId;
			r->qty = fill.qty;
			r->price = fill.price;
			r->commission = fill.commission;
			wrapper->onRspQryFill(r, isLast);
		}

		virtual void onRspQryPosition(const TLCClientApi::Position &position, bool isLast)
		{
			Position^ r = gcnew Position();
			r->account = gcnew String(position.account);
		}
	};


	public ref class TradeApiWrapper
	{
	public:
		TLCClientApi::TradeApi &api;
		TradeApiWrapper(TradeSpiWrapper^ spi) : api(TLCClientApi::TradeApi::create(*spi->imp)) {}

		void start(String^ address)
		{
			api.start((const char*)(Marshal::StringToHGlobalAnsi(address)).ToPointer());
		}

		ReturnType login(LoginRequest^ login)
		{
			TLCClientApi::LoginRequest r;
			strcpy_s(r.username, Util::toCString(login->username));
			strcpy_s(r.password, Util::toCString(login->password));
			return (ReturnType)api.login(r);
		}

		ReturnType logout()
		{
			return (ReturnType)api.logout();
		}

		ReturnType qryAccount()
		{
			return (ReturnType)api.qryAccount();
		}

		ReturnType newOrder(OrderRequest^ orderRequest)
		{
			TLCClientApi::OrderRequest r;
			strcpy_s(r.account, Util::toCString(orderRequest->account));
			strcpy_s(r.symbol, Util::toCString(orderRequest->symbol));
			r.timeInForce = (TLCClientApi::TimeInForce)orderRequest->timeInForce;
			r.side = (TLCClientApi::Side)orderRequest->side;
			r.qty = orderRequest->qty;
			r.price = orderRequest->price;
			r.positionOffset = (TLCClientApi::PositionOffset)orderRequest->positionOffset;
			r.clOrdId = orderRequest->clOrdId;
			return (ReturnType)api.newOrder(r);
		}

		ReturnType cancelOrder(OrderCancelRequest^ orderCancelRequest)
		{
			TLCClientApi::OrderCancelRequest r;
			r.id.clOrdId = orderCancelRequest->id->clOrdId;
			r.id.ordId = orderCancelRequest->id->ordId;
			return (ReturnType)api.cancelOrder(r);
		}

		ReturnType qryOrder(QryOrderRequest^ qryOrderRequest)
		{
			TLCClientApi::QryOrderRequest r;
			strcpy_s(r.account, Util::toCString(qryOrderRequest->account));
			return (ReturnType)api.qryOrder(r);
		}

		ReturnType qryFill(QryFillRequest^ qryFillRequest)
		{
			TLCClientApi::QryFillRequest r;
			strcpy_s(r.account, Util::toCString(qryFillRequest->account));
			return (ReturnType)api.qryFill(r);
		}

		ReturnType qryPosition(QryPositionRequest^ qryPositionRequest)
		{
			TLCClientApi::QryPositionRequest r;
			strcpy_s(r.account, Util::toCString(qryPositionRequest->account));
			return (ReturnType)api.qryPosition(r);
		}
	};
}
